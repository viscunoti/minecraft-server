package com.viscunoti.minecraft.warps;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

import org.bukkit.Location;

import com.google.common.collect.Maps;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class WarpManager extends Manager {
	private Map<String, Warp> warps = Maps.newHashMap();

	public WarpManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	public void reload() {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = database.prepareStatement("SELECT * FROM warps");
			rs = database.query(st);
			while (rs.next()) {
				Warp w = new Warp(this, rs.getString("name"), rs.getString("world"), rs.getDouble("x"),
						rs.getDouble("y"), rs.getDouble("z"));
				w.setPermission(rs.getString("permission"));
				warps.put(w.getName().toLowerCase(), w);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close(rs);
			database.close(st);
		}
	}

	public Warp getWarp(String name) {
		if (name != null && !name.isEmpty())
			return warps.get(name.toLowerCase());
		return null;
	}

	public void addWarp(String name, Location location) {
		Warp warp = new Warp(this, name, location.getWorld().getName(), location.getX(), location.getY(),
				location.getZ());
		warps.put(warp.getName().toLowerCase(), warp);
		save(warp);
	}

	public Warp delete(String name) {
		String sql = "DELETE FROM warps WHERE name = ?";
		database.asyncUpdate(sql, new Object[] { name });
		return warps.remove(name.toLowerCase());
	}

	public void save(Warp warp) {
		if (warp != null) {
			String sql = "INSERT INTO warps (name, world, x, y, z, permission) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE name = VALUES(name), world = VALUES(world), x = VALUES(x), y = VALUES(y), z = VALUES(z), permission = VALUES(permission)";
			database.asyncUpdate(sql, new Object[] { warp.getName(), warp.getWorldName(), warp.getX(), warp.getY(),
					warp.getZ(), warp.getPermission() });
		}
	}
}
