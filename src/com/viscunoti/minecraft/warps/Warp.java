package com.viscunoti.minecraft.warps;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Warp {
	private WarpManager warpManager;
	private String name, world, permission = null;
	private double x, y, z;

	protected Warp(WarpManager warpManager, String name, String world, double x, double y, double z) {
		this.warpManager = warpManager;
		this.name = name;
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Location getLocation() {
		return new Location(Bukkit.getWorld(world), x, y, z);
	}

	public void delete() {
		warpManager.delete(name);
	}

	public void save() {
		warpManager.save(this);
	}

	public String getName() {
		return name;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getWorldName() {
		return world;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	public boolean hasPermissionRequirement() {
		return permission != null && !permission.isEmpty();
	}
}
