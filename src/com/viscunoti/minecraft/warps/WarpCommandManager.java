package com.viscunoti.minecraft.warps;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.spells.SpellCastManager;

public class WarpCommandManager extends CommandManager {
	private WarpManager warpManager;
	private SpellCastManager spellCastManager;

	public WarpCommandManager(WarpManager warpManager, SpellCastManager spellCastManager) {
		this.warpManager = warpManager;
		this.spellCastManager = spellCastManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (args.length == 1) {
				Warp warp = warpManager.getWarp(args[0]);
				if (warp != null && (!warp.hasPermissionRequirement() || p.hasPermission(warp.getPermission()))) {
					spellCastManager.cast(p, 60, new Runnable() {
						public void run() {
							p.teleport(warp.getLocation(), TeleportCause.COMMAND);
						}
					});
					p.sendMessage("Warping to " + warp.getName() + ".  Do not move until the cast is complete.");
				} else {
					p.sendMessage(ChatColor.GRAY + "Warp not found.");
				}
				return true;
			} else if (args.length == 2 && p.hasPermission("viscunoti.warps.edit")) {
				if (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("edit")
						|| args[0].equalsIgnoreCase("add")) {
					warpManager.addWarp(args[1], p.getLocation());
					p.sendMessage("Added warp " + args[1] + ".");
					return true;
				} else if (args[0].equalsIgnoreCase("delete")) {
					Warp deleted = warpManager.delete(args[1]);
					if (deleted != null)
						p.sendMessage("Deleted warp " + deleted.getName() + ".");
					else
						p.sendMessage(ChatColor.GRAY + "Warp not found.");
					return true;
				}
			}
		}
		return false;
	}
}
