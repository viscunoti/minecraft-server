package com.viscunoti.minecraft;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.viscunoti.minecraft.database.Database;

public class PropertiesManager extends Manager {
	public PropertiesManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	public String getProperty(String key) {
		String sql = "SELECT `value` FROM `properties` WHERE `key` = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = database.prepareStatement(sql);
			st.setString(1, key);
			rs = database.query(st);
			if (rs.next())
				return rs.getString("value");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close(rs);
			database.close(st);
		}
		return null;
	}

	public void setProperty(String key, String value) {
		String sql = "INSERT INTO `properties` (`key`, `value`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)";
		database.asyncUpdate(sql, new Object[] { key, value });
	}
}
