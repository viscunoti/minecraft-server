package com.viscunoti.minecraft;

import com.viscunoti.minecraft.database.Database;

public abstract class Manager {
	protected ViscunotiPlugin plugin;
	protected Database database;
	protected int tickRate = 20;

	public Manager(ViscunotiPlugin plugin, Database database) {
		this.plugin = plugin;
		this.database = database;
	}

	public void tick() {
	}

	public void init() {
	}

	public void reload() {
	}

	public long getTickRate() {
		return tickRate;
	}
}
