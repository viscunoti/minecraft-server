package com.viscunoti.minecraft.menus;

import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import com.google.common.collect.Maps;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class MenuManager extends Manager implements Listener {
	private Map<UUID, Menu> openMenus = Maps.newHashMap();

	public MenuManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryClick(InventoryClickEvent e) {
		Menu menu = openMenus.get(e.getWhoClicked().getUniqueId());
		if (menu != null && e.getWhoClicked() instanceof Player) {
			Player p = (Player) e.getWhoClicked();
			MenuItem item = menu.getItem(e.getRawSlot());
			if (item != null)
				item.onClick(p, menu);
			if (e.isShiftClick() || e.getRawSlot() < menu.getSize())
				e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryDrag(InventoryDragEvent e) {
		if (openMenus.containsKey(e.getWhoClicked().getUniqueId()))
			e.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryClose(InventoryCloseEvent e) {
		openMenus.remove(e.getPlayer().getUniqueId());
	}

	public void openMenu(Player player, Menu menu) {
		if (player != null && menu != null) {
			player.openInventory(menu.getInventory());
			openMenus.put(player.getUniqueId(), menu);
		}
	}
}
