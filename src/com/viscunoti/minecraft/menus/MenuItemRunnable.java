package com.viscunoti.minecraft.menus;

import org.bukkit.entity.Player;

public interface MenuItemRunnable {
	public void run(Player player, Menu menu);
}
