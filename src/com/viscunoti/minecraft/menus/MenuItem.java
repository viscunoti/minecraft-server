package com.viscunoti.minecraft.menus;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MenuItem {
	private ItemStack display;
	private MenuItemRunnable onClick;

	public MenuItem(ItemStack display, MenuItemRunnable onClick) {
		this.display = display;
		this.onClick = onClick;
	}

	public ItemStack getDisplayItem() {
		return display.clone();
	}

	public void onClick(Player player, Menu menu) {
		if (onClick != null)
			onClick.run(player, menu);
	}

}
