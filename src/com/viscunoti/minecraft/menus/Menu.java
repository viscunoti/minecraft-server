package com.viscunoti.minecraft.menus;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.ImmutableList;

public class Menu {
	private MenuManager menuManager;
	private String title;
	private MenuItem[] items;

	public Menu(MenuManager menuManager, String title, int slots) {
		this.menuManager = menuManager;
		this.title = title;
		this.items = new MenuItem[slots];
	}

	public void fill(MenuItem item) {
		for (int i = 0; i < items.length; i++)
			items[i] = item;
	}

	public void setItem(int index, MenuItem item) {
		if (index >= 0 && index < items.length)
			items[index] = item;
	}

	public MenuItem getItem(int index) {
		if (index >= 0 && index < items.length)
			return items[index];
		return null;
	}

	public Inventory getInventory() {
		Inventory inventory = Bukkit.createInventory(null, items.length, title);
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null)
				inventory.setItem(i, items[i].getDisplayItem());
		}
		return inventory;
	}

	public void open(Player player) {
		menuManager.openMenu(player, this);
	}

	public String getTitle() {
		return title;
	}

	public List<MenuItem> getItems() {
		return ImmutableList.copyOf(Arrays.asList(items));
	}

	public int getSize() {
		return items.length;
	}
}
