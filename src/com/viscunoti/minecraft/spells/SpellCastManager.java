package com.viscunoti.minecraft.spells;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import com.google.common.collect.Maps;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.PacketUtils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class SpellCastManager extends Manager implements Listener {
	private Map<UUID, SpellCast> casts = Maps.newHashMap();

	public SpellCastManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
		this.tickRate = 4;
	}

	@EventHandler
	private void onEntityDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			SpellCast cast = casts.get(p.getUniqueId());
			if (cast != null && cast.isDamageInterrupts()) {
				p.sendMessage(ChatColor.GRAY + "Interrupted.");
				casts.remove(p.getUniqueId());
				PacketUtils.updateExp(p);
			}
		}
	}

	public void cast(Player player, int ticks, Runnable onComplete) {
		if (casts.containsKey(player.getUniqueId())) {
			casts.remove(player.getUniqueId());
		} else {
			casts.put(player.getUniqueId(), new SpellCast(player, ticks, onComplete));
		}
	}

	public void tick() {
		Iterator<SpellCast> iterator = casts.values().iterator();
		while (iterator.hasNext()) {
			SpellCast cast = iterator.next();
			boolean complete = cast.tick(tickRate);
			if (complete)
				iterator.remove();
		}
	}
}
