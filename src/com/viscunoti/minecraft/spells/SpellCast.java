package com.viscunoti.minecraft.spells;

import net.minecraft.server.v1_8_R3.PacketPlayOutExperience;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.viscunoti.minecraft.PacketUtils;

public class SpellCast {
	private int tick, totalTicks;
	private Player player;
	private Runnable onComplete;
	private Location startLocation;
	private boolean movementInterrupts = true, damageInterrupts = true;

	public SpellCast(Player player, int ticks, Runnable onComplete) {
		this.player = player;
		this.startLocation = player.getLocation();
		this.totalTicks = ticks;
		this.onComplete = onComplete;
	}

	public boolean tick() {
		return tick(1);
	}

	public boolean tick(int steps) {
		tick += steps;
		if (!player.isOnline()) {
			return true;
		}
		if (movementInterrupts && startLocation.distanceSquared(player.getLocation()) > 0.25) {
			player.sendMessage(ChatColor.GRAY + "Interrupted.");
			PacketUtils.updateExp(player);
			return true;
		}
		float xp = tick / (float) totalTicks;
		if (tick >= totalTicks) {
			xp = player.getExp();
			if (onComplete != null) {
				try {
					onComplete.run();
				} catch (Exception e) {
					e.printStackTrace();
				}
				PacketUtils.updateExp(player);
				return true;
			}
		}
		PacketPlayOutExperience packet = new PacketPlayOutExperience(xp, 1000, player.getLevel());
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		return false;
	}

	public boolean isMovementInterrupts() {
		return movementInterrupts;
	}

	public void setMovementInterrupts(boolean movementInterrupts) {
		this.movementInterrupts = movementInterrupts;
	}

	public boolean isDamageInterrupts() {
		return damageInterrupts;
	}

	public void setDamageInterrupts(boolean damageInterrupts) {
		this.damageInterrupts = damageInterrupts;
	}
}
