package com.viscunoti.minecraft.areas;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.Utils;

public class AreaCommandManager extends CommandManager {
	private AreaManager areaManager;

	public AreaCommandManager(AreaManager areaManager) {
		this.areaManager = areaManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (args.length == 9 && args[0].equalsIgnoreCase("create") && sender.hasPermission("viscunoti.areas.create")) {
			try {
				Area area = new Area(args[1].replace('_', ' '), args[2], Double.parseDouble(args[3]),
						Double.parseDouble(args[4]), Double.parseDouble(args[5]), Double.parseDouble(args[6]),
						Double.parseDouble(args[7]), Double.parseDouble(args[8]));
				boolean success = areaManager.registerArea(area);
				if (success) {
					sender.sendMessage("Created area " + area.getName() + ".");
				} else
					sender.sendMessage(ChatColor.GRAY + "Failed to create area with name " + args[1] + ".");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (args.length >= 4 && args[0].equalsIgnoreCase("edit") && sender.hasPermission("viscunoti.areas.edit")) {
			Area area = areaManager.getArea(args[1].replace('_', ' '));
			if (area != null) {
				if (args[2].equalsIgnoreCase("name")) {
					String originalName = area.getName();
					area.setName(args[3].replace('_', ' '));
					area.save();
					sender.sendMessage("Renamed area from " + originalName + " to " + area.getName() + ".");
					return true;
				} else if (args[2].equalsIgnoreCase("location") && args.length == 10) {
					try {
						double xmin = Double.parseDouble(args[4]), xmax = Double.parseDouble(args[5]), ymin = Double
								.parseDouble(args[6]), ymax = Double.parseDouble(args[7]), zmin = Double
								.parseDouble(args[8]), zmax = Double.parseDouble(args[9]);
						area.setWorld(args[3]);
						area.setXmin(xmin);
						area.setXmax(xmax);
						area.setYmin(ymin);
						area.setYmax(ymax);
						area.setZmin(zmin);
						area.setZmax(zmax);
						sender.sendMessage("Updated location of area " + area.getName() + ".");
						return true;
					} catch (Exception e) {
					}
				} else if (args[2].equalsIgnoreCase("owner")) {
					OfflinePlayer owner;
					if (args[3].trim().isEmpty())
						owner = null;
					else
						owner = Bukkit.getOfflinePlayer(args[3]);
					area.setOwner(owner);
					area.save();
					sender.sendMessage("Set owner of " + area.getName() + " to " + owner.getName() + ".");
					return true;
				} else if (args[2].equalsIgnoreCase("pvp")) {
					area.setPvpEnabled(Utils.toBoolean(args[3]));
					area.save();
					sender.sendMessage("Set pvp to " + area.isPvpEnabled() + " for area " + area.getName() + ".");
					return true;
				} else if (args[2].equalsIgnoreCase("protected")) {
					area.setProtectedArea(Utils.toBoolean(args[3]));
					area.save();
					sender.sendMessage("Set protected to " + area.isProtectedArea() + " for area " + area.getName()
							+ ".");
					return true;
				} else if (args[2].equalsIgnoreCase("enter_announcement")) {
					String announcement = Joiner.on(' ').join(Arrays.copyOfRange(args, 3, args.length));
					area.setEnterAnnouncement(announcement);
					area.save();
					sender.sendMessage("Set enter announcement to " + area.getEnterAnnouncement() + " for area "
							+ area.getName() + ".");
					return true;
				} else if (args[2].equalsIgnoreCase("exit_announcement")) {
					String announcement = Joiner.on(' ').join(Arrays.copyOfRange(args, 3, args.length));
					area.setExitAnnouncement(announcement);
					area.save();
					sender.sendMessage("Set exit announcement to " + area.getExitAnnouncement() + " for area "
							+ area.getName() + ".");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.GRAY + "Area not found.");
			}
		} else if (args.length >= 3 && args[0].equalsIgnoreCase("members")) {
			Area area = areaManager.getArea(args[2].replace('_', ' '));
			if (area != null
					&& (area.getOwner() != null && sender instanceof Player
							&& area.getOwner().equals(((Player) sender).getUniqueId()) || sender
								.hasPermission("viscunoti.areas.editmembers"))) {
				if (args[1].equalsIgnoreCase("view") || args[1].equalsIgnoreCase("show")) {
					Set<UUID> members = area.getMembers();
					List<String> messages = Lists.newArrayList();
					StringBuilder current = new StringBuilder();
					for (UUID id : members) {
						OfflinePlayer member = Bukkit.getOfflinePlayer(id);
						if (current.length() > 25) {
							messages.add(current.toString());
							current = new StringBuilder();
						}
						if (current.length() > 0)
							current.append(", ");
						current.append(member.getName());
					}
					if (current.length() > 0)
						messages.add(current.toString());
					if (area.getOwner() != null) {
						OfflinePlayer owner = Bukkit.getOfflinePlayer(area.getOwner());
						sender.sendMessage("Owner of area " + area.getName() + ": " + owner.getName());
					}
					if (!messages.isEmpty()) {
						sender.sendMessage("Members of area " + area.getName() + ":");
						sender.sendMessage(messages.toArray(new String[messages.size()]));
					}
					if (area.getOwner() == null && messages.isEmpty()) {
						sender.sendMessage(area.getName() + " has no owner or members.");
					}
					return true;
				} else if (args[1].equalsIgnoreCase("add")) {
					OfflinePlayer member = Bukkit.getOfflinePlayer(args[3]);
					if (member != null) {
						if (!member.getUniqueId().equals(area.getOwner())) {
							if (area.addMember(member)) {
								sender.sendMessage("Added " + member.getName() + " to " + area.getName() + ".");
							} else {
								sender.sendMessage(member.getName() + " is already a member of " + area.getName() + ".");
							}
						} else {
							sender.sendMessage(ChatColor.RED
									+ "The owner of an area can not be a member of the same area.");
						}
					} else {
						sender.sendMessage(ChatColor.GRAY + "Player does not exist.");
					}
					return true;
				} else if (args[1].equalsIgnoreCase("remove") || args[1].equalsIgnoreCase("delete")) {
					OfflinePlayer member = Bukkit.getOfflinePlayer(args[3]);
					if (member != null) {
						if (area.removeMember(member)) {
							sender.sendMessage("Removed " + member.getName() + " from " + area.getName() + ".");
						} else {
							sender.sendMessage(ChatColor.GRAY + member.getName() + " is not a member of "
									+ area.getName() + ".");
						}
					} else {
						sender.sendMessage(ChatColor.GRAY + "Player not found.");
					}
					return true;
				}
			}
		} else if (args.length == 2 && (args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("remove"))
				&& sender.hasPermission("viscunoti.areas.delete")) {
			Area area = areaManager.getArea(args[1]);
			if (area != null) {
				areaManager.deleteArea(area);
				sender.sendMessage("Deleted area " + area.getName() + ".");
			} else {
				sender.sendMessage(ChatColor.GRAY + "Area does not exist.");
			}
			return true;
		} else if (args.length == 1 && sender instanceof Player || args.length == 2
				&& sender.hasPermission("viscunoti.areas.viewuser")) {
			if (args[0].equalsIgnoreCase("member")) {
				OfflinePlayer target = args.length == 2 ? Bukkit.getOfflinePlayer(args[1]) : (Player) sender;
				if (target != null) {
					List<Area> areas = areaManager.getAreas();
					sender.sendMessage(target.getName() + " is a member of:");
					for (Area a : areas) {
						if (a.isMember(target))
							sender.sendMessage(a.getName());
					}
				} else
					sender.sendMessage(ChatColor.GRAY + "Player not found.");
				return true;
			} else if (args[0].equalsIgnoreCase("owner")) {
				OfflinePlayer target = args.length == 2 ? Bukkit.getOfflinePlayer(args[1]) : (Player) sender;
				if (target != null) {
					List<Area> areas = areaManager.getAreas();
					sender.sendMessage(target.getName() + " is the owner of:");
					for (Area a : areas) {
						if (target.getUniqueId().equals(a.getOwner()))
							sender.sendMessage(a.getName());
					}
				} else
					sender.sendMessage(ChatColor.GRAY + "Player not found.");
				return true;
			}
		} else if (args.length == 0 && sender instanceof Player) {
			Player p = (Player) sender;
			List<Area> areas = areaManager.getAreasContaining(p.getLocation());
			p.sendMessage("Areas you are currently in:");
			for (Area a : areas) {
				p.sendMessage(a.getName());
			}
			return true;
		}
		return false;
	}
}
