package com.viscunoti.minecraft.areas.homes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.areas.Area;
import com.viscunoti.minecraft.areas.AreaManager;
import com.viscunoti.minecraft.database.Database;

public class HomeManager extends Manager {
	private AreaManager areaManager;

	public HomeManager(ViscunotiPlugin plugin, Database database, AreaManager areaManager) {
		super(plugin, database);
		this.areaManager = areaManager;
	}

	public boolean createHome(Player player) {
		Location l = player.getLocation();
		FileConfiguration cfg = plugin.getConfig();
		double homeRadius = cfg.getDouble("Home.Radius");
		double xmin = l.getX() - homeRadius, xmax = l.getX() + homeRadius, ymin = 25, ymax = 1024, zmin = l.getZ()
				- homeRadius, zmax = l.getZ() + homeRadius;

		String sql = "SELECT * FROM minecraft.areas WHERE owner_id != ? AND world = ? AND xmin < ? AND ? < xmax AND "
				+ "ymin < ? AND ? < ymax AND zmin < ? AND ? < zmax AND protected > 0";
		boolean cancel = false;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = database.prepareStatement(sql);
			st.setString(1, player.getUniqueId().toString());
			st.setString(2, l.getWorld().getName());
			st.setDouble(3, xmax);
			st.setDouble(4, xmin);
			st.setDouble(5, ymax);
			st.setDouble(6, ymin);
			st.setDouble(7, zmax);
			st.setDouble(8, zmin);
			rs = database.query(st);
			cancel = rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close(rs);
			database.close(st);
		}

		if (cancel)
			return false;
		Area area = new Area(player.getName() + "'s home", l.getWorld().getName(), xmin, xmax, ymin, ymax, zmin, zmax);
		area.setOwner(player.getUniqueId());
		area.setPvpEnabled(false);
		area.setProtectedArea(true);
		sql = "SELECT area_id FROM homes WHERE user_id = ?";

		try {
			st = database.prepareStatement(sql);
			st.setString(1, player.getUniqueId().toString());
			rs = database.query(st);
			if (rs.next()) {
				areaManager.deleteArea(UUID.fromString(rs.getString("area_id")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close(rs);
			database.close(st);
		}

		areaManager.registerArea(area);
		area.setProperty("protect_chests", true);

		sql = "INSERT INTO homes (user_id, area_id, world, x, y, z) VALUES (?, ?, ?, ?, ?, ?) "
				+ "ON DUPLICATE KEY UPDATE area_id = VALUES(area_id), world = VALUES(world), x = VALUES(x), y = VALUES(y), z = VALUES(z)";
		database.asyncUpdate(sql, new Object[] { player.getUniqueId().toString(), area.getUniqueId().toString(),
				l.getWorld().getName(), l.getX(), l.getY(), l.getZ() });
		return true;
	}

	public Location getHomeLocation(Player player) {
		String sql = "SELECT world, x, y, z FROM homes WHERE user_id = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = database.prepareStatement(sql);
			st.setString(1, player.getUniqueId().toString());
			rs = database.query(st);
			if (rs.next()) {
				return new Location(Bukkit.getWorld(rs.getString("world")), rs.getDouble("x"), rs.getDouble("y"),
						rs.getDouble("z"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close(rs);
			database.close(st);
		}
		return null;
	}
}
