package com.viscunoti.minecraft.areas.homes;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.spells.SpellCastManager;

public class HomeCommandManager extends CommandManager {
	private HomeManager homeManager;
	private SpellCastManager spellCastManager;

	public HomeCommandManager(HomeManager homeManager, SpellCastManager spellCastManager) {
		this.homeManager = homeManager;
		this.spellCastManager = spellCastManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (args.length == 0) {
				spellCastManager.cast(p, 60, new Runnable() {
					public void run() {
						p.teleport(homeManager.getHomeLocation(p), TeleportCause.COMMAND);
					}
				});
				p.sendMessage("Teleporting home.  Do not move until the cast is complete.");
				return true;
			} else if (args.length == 1 && args[0].equalsIgnoreCase("set")) {
				spellCastManager.cast(p, 200, new Runnable() {
					public void run() {
						if (homeManager.createHome(p)) {
							Location l = p.getLocation();
							p.sendMessage("Set home to " + Math.round(l.getX()) + ", " + Math.round(l.getY()) + ", "
									+ Math.round(l.getZ()));
						} else {
							p.sendMessage(ChatColor.GRAY + "You can not set your home here.");
						}
					}
				});
				p.sendMessage("Setting home to your location.  Do not move until the cast is complete.");
				return true;
			}
		}
		return false;
	}
}
