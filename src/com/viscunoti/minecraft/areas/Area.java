package com.viscunoti.minecraft.areas;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class Area {
	private double xmin, xmax, ymin, ymax, zmin, zmax;
	private boolean pvp, protectedArea;
	private String world, enterAnnouncement = "You entered ##name##.", exitAnnouncement = "You left ##name##.";
	private UUID owner, id;
	private AreaManager areaManager;
	String name;
	Set<UUID> members = Sets.newHashSet();
	Map<String, Boolean> properties = Maps.newHashMap();

	public Area(String name, String world, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
		this(null, name, world, xmin, xmax, ymin, ymax, zmin, zmax, null);
	}

	public Area(AreaManager areaManager, String name, String world, double xmin, double xmax, double ymin, double ymax,
			double zmin, double zmax, UUID id) {
		if (name == null || world == null)
			throw new NullPointerException();
		setArea(world, xmin, xmax, ymin, ymax, zmin, zmax);
		this.areaManager = areaManager;
		this.name = name;
		this.id = id;
	}

	public void save() {
		if (areaManager != null)
			areaManager.saveArea(this);
		else
			throw new NullPointerException("Area must be registered before saving");
	}

	public boolean isInArea(Location l) {
		if (l != null) {
			double x = l.getX(), y = l.getY(), z = l.getZ();
			return l.getWorld().getName().equals(world) && x >= xmin && x <= xmax && y >= ymin && y <= ymax
					&& z >= zmin && z <= zmax;
		}
		return false;
	}

	public void setArea(String world, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
		this.world = world;
		this.xmin = xmin;
		this.xmax = xmax;
		this.ymin = ymin;
		this.ymax = ymax;
		this.zmin = zmin;
		this.zmax = zmax;
	}

	public double getAreaSize() {
		return (xmax - xmin) * (ymax - ymin) * (zmax - zmin);
	}

	public double getAreaSizeIgnoreY() {
		return (xmax - xmin) * (zmax - zmin);
	}

	public ImmutableSet<UUID> getMembers() {
		return ImmutableSet.copyOf(members);
	}

	public boolean addMember(OfflinePlayer player) {
		if (player != null)
			return addMember(player.getUniqueId());
		return false;
	}

	public boolean addMember(UUID playerId) {
		if (areaManager != null)
			return areaManager.addMember(this, playerId);
		else
			throw new NullPointerException("Area must be registered before adding member");
	}

	public boolean removeMember(OfflinePlayer player) {
		if (player != null)
			return removeMember(player.getUniqueId());
		return false;
	}

	public boolean removeMember(UUID playerId) {
		if (areaManager != null)
			return areaManager.removeMember(this, playerId);
		else
			throw new NullPointerException("Area must be registered before saving");
	}

	public boolean isMember(OfflinePlayer player) {
		if (player != null)
			return members.contains(player.getUniqueId()) || player.getUniqueId().equals(owner);
		return false;
	}

	public boolean hasProperty(String property) {
		return properties.containsKey(property);
	}

	public boolean getProperty(String property) {
		Boolean value = properties.get(property);
		if (value != null)
			return value;
		return false;
	}

	public void setProperty(String property, boolean value) {
		areaManager.addProperty(this, property, value);
	}

	public boolean isPvpEnabled() {
		return pvp;
	}

	public void setPvpEnabled(boolean pvp) {
		this.pvp = pvp;
	}

	public boolean isProtectedArea() {
		return protectedArea;
	}

	public void setProtectedArea(boolean protectedArea) {
		this.protectedArea = protectedArea;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		areaManager.rename(this, name);
	}

	public String getEnterAnnouncement() {
		if (hasEnterAnnouncement())
			return enterAnnouncement.replace("##name##", name);
		else
			return null;
	}

	public String getExitAnnouncement() {
		if (hasExitAnnouncement())
			return exitAnnouncement.replace("##name##", name);
		else
			return null;
	}

	public void setEnterAnnouncement(String announcement) {
		enterAnnouncement = announcement;
	}

	public void setExitAnnouncement(String announcement) {
		exitAnnouncement = announcement;
	}

	public UUID getOwner() {
		return owner;
	}

	public void setOwner(UUID owner) {
		this.owner = owner;
	}

	public void setOwner(OfflinePlayer owner) {
		if (owner != null)
			setOwner(owner.getUniqueId());
		else
			this.owner = null;
	}

	public void setWorld(String world) {
		this.world = world;
	}

	public String getWorld() {
		return world;
	}

	public UUID getUniqueId() {
		if (id == null)
			id = UUID.randomUUID();
		return id;
	}

	public double getXmin() {
		return xmin;
	}

	public void setXmin(double xmin) {
		this.xmin = xmin;
	}

	public double getXmax() {
		return xmax;
	}

	public void setXmax(double xmax) {
		this.xmax = xmax;
	}

	public double getYmin() {
		return ymin;
	}

	public void setYmin(double ymin) {
		this.ymin = ymin;
	}

	public double getYmax() {
		return ymax;
	}

	public void setYmax(double ymax) {
		this.ymax = ymax;
	}

	public double getZmin() {
		return zmin;
	}

	public void setZmin(double zmin) {
		this.zmin = zmin;
	}

	public double getZmax() {
		return zmax;
	}

	public void setZmax(double zmax) {
		this.zmax = zmax;
	}

	public AreaManager getAreaManager() {
		return areaManager;
	}

	public void setAreaManager(AreaManager areaManager) {
		this.areaManager = areaManager;
	}

	public boolean hasEnterAnnouncement() {
		return enterAnnouncement != null && !enterAnnouncement.isEmpty();
	}

	public boolean hasExitAnnouncement() {
		return exitAnnouncement != null && !exitAnnouncement.isEmpty();
	}

	public boolean equals(Object obj) {
		if (obj instanceof Area)
			return ((Area) obj).getUniqueId().equals(getUniqueId());
		return false;
	}
}
