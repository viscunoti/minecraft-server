package com.viscunoti.minecraft.areas;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.PluginManager;

import com.google.common.collect.ImmutableSet;
import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class AreaProtectionManager extends Manager implements Listener {
	private AreaManager areaManager;
	private HorseProtectionManager horseProtectionManager;
	private Set<String> protectChest, protectPhysical, protectEntities, protectItems;

	public AreaProtectionManager(ViscunotiPlugin plugin, Database database, AreaManager areaManager) {
		super(plugin, database);
		this.areaManager = areaManager;
		horseProtectionManager = new HorseProtectionManager(plugin, database);
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(horseProtectionManager, plugin);
	}

	public void reload() {
		protectChest = ImmutableSet.copyOf(plugin.getConfig().getStringList("Area.Chest Protection"));
		protectPhysical = ImmutableSet.copyOf(plugin.getConfig().getStringList("Area.Physical Protection"));
		protectEntities = ImmutableSet.copyOf(plugin.getConfig().getStringList("Area.Protected Entities"));
		protectItems = ImmutableSet.copyOf(plugin.getConfig().getStringList("Area.Protected Items"));
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onEntityChangeBlock(EntityChangeBlockEvent e) {
		Entity entity = e.getEntity();
		if (entity.hasMetadata("viscunoti.created_location")) {
			Location created = (Location) entity.getMetadata("viscunoti.created_location").get(0).value();
			List<Area> initialAreas = areaManager.getAreasContaining(created);
			List<Area> currentAreas = areaManager.getAreasContaining(entity.getLocation());
			currentAreas.removeAll(initialAreas);
			for (Area a : currentAreas) {
				if (a.isProtectedArea()) {
					e.setCancelled(true);
					break;
				}
			}
		} else {
			entity.setMetadata("viscunoti.created_location", new FixedMetadataValue(plugin, entity.getLocation()));
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		Entity damagerEntity = Utils.getLastDamager(e);
		if (damagerEntity instanceof Player) {
			Player damager = (Player) damagerEntity;
			Area smallestArea = areaManager.getSmallestAreaContaining(e.getEntity().getLocation());
			if (smallestArea != null && smallestArea.isProtectedArea() && !smallestArea.isMember(damager)
					&& protectEntities.contains(e.getEntityType().toString())) {
				e.setCancelled(true);
			}
			if (e.getEntity() instanceof Player) {
				Player damaged = (Player) e.getEntity();
				if (smallestArea != null && !smallestArea.isPvpEnabled()) {
					if (!smallestArea.isMember(damager) || smallestArea.isMember(damaged)) {
						e.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onVehicleBreak(VehicleDestroyEvent e) {
		Area smallestArea = areaManager.getSmallestAreaContaining(e.getVehicle().getLocation());
		if (smallestArea != null && smallestArea.isProtectedArea() && e.getAttacker() instanceof Player
				&& !smallestArea.isMember((Player) e.getAttacker())) {
			e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onHangingBreakByEntity(HangingBreakByEntityEvent e) {
		Area smallestArea = areaManager.getSmallestAreaContaining(e.getEntity().getLocation());
		if (smallestArea != null && smallestArea.isProtectedArea() && e.getRemover() instanceof Player
				&& !smallestArea.isMember((Player) e.getRemover())) {
			e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onHangingPlace(HangingPlaceEvent e) {
		Area smallestArea = areaManager.getSmallestAreaContaining(e.getBlock().getLocation());
		if (smallestArea != null && smallestArea.isProtectedArea() && !smallestArea.isMember(e.getPlayer())) {
			e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onEntityInteract(PlayerInteractEntityEvent e) {
		if (protectEntities.contains(e.getRightClicked().getType().toString())) {
			Area smallestArea = areaManager.getSmallestAreaContaining(e.getRightClicked().getLocation());
			if (smallestArea != null && smallestArea.isProtectedArea() && !smallestArea.isMember(e.getPlayer())) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onInteract(PlayerInteractEvent e) {
		Block block = e.getClickedBlock();
		// Block protection
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && protectChest.contains(block.getType().toString())) {
			Area area = areaManager.getSmallestAreaContaining(block.getLocation());
			if (area != null) {
				if (area.getProperty("protect_chests") && !area.isMember(e.getPlayer())) {
					List<String[]> signs = Utils.getSignText(block);
					boolean shared = false;
					signloop: for (String[] sign : signs) {
						StringBuilder sb = new StringBuilder();
						for (String s : sign) {
							sb.append(s);
						}
						String s = sb.toString().toLowerCase();
						if (s.contains("[public]") || s.contains("[" + e.getPlayer().getName().toLowerCase() + "]")) {
							shared = true;
							break signloop;
						}
					}
					if (!shared) {
						e.setUseInteractedBlock(Result.DENY);
						e.getPlayer().sendMessage(
								ChatColor.GRAY + "You don't have permission to use this "
										+ Language.translate(block.getType().toString()) + ".");
					}
				}
			}
		} else if (e.getAction().equals(Action.PHYSICAL) && protectPhysical.contains(block.getType().toString())) {
			Area area = areaManager.getSmallestAreaContaining(block.getLocation());
			if (area != null && area.isProtectedArea() && !area.isMember(e.getPlayer())) {
				e.setCancelled(true);
			}
		}
		// Place entity protection
		if ((e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
				&& e.getItem() != null && protectItems.contains(e.getItem().getType().toString())) {
			Area area = areaManager.getSmallestAreaContaining(block != null ? block.getLocation() : e.getPlayer()
					.getLocation());
			if (area != null && area.isProtectedArea() && !area.isMember(e.getPlayer())) {
				e.setUseItemInHand(Result.DENY);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onBucketFill(PlayerBucketEmptyEvent e) {
		List<Area> areas = areaManager.getAreasContaining(e.getBlockClicked().getLocation());
		for (Area a : areas) {
			if (a.isProtectedArea() && !a.isMember(e.getPlayer())) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onBucketFill(PlayerBucketFillEvent e) {
		List<Area> areas = areaManager.getAreasContaining(e.getBlockClicked().getLocation());
		for (Area a : areas) {
			if (a.isProtectedArea() && !a.isMember(e.getPlayer())) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onBlockPlace(BlockPlaceEvent e) {
		List<Area> areas = areaManager.getAreasContaining(e.getBlock().getLocation());
		for (Area a : areas) {
			if (a.isProtectedArea() && !a.isMember(e.getPlayer())) {
				e.setBuild(false);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onBlockBreak(BlockBreakEvent e) {
		List<Area> areas = areaManager.getAreasContaining(e.getBlock().getLocation());
		for (Area a : areas) {
			if (a.isProtectedArea() && !a.isMember(e.getPlayer())) {
				e.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onBlockIgnite(BlockIgniteEvent e) {
		if (e.getCause().equals(IgniteCause.SPREAD) || e.getCause().equals(IgniteCause.FLINT_AND_STEEL)) {
			List<Area> areas = areaManager.getAreasContaining(e.getBlock().getLocation());
			for (Area a : areas) {
				if (a.isProtectedArea() && (e.getPlayer() == null || !a.isMember(e.getPlayer()))) {
					e.setCancelled(true);
					return;
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onEntityExplode(EntityExplodeEvent e) {
		Iterator<Block> it = e.blockList().iterator();
		while (it.hasNext()) {
			Block block = it.next();
			List<Area> areas = areaManager.getAreasContaining(block.getLocation());
			for (Area a : areas) {
				if (a.isProtectedArea()) {
					if (e.getEntity() instanceof TNTPrimed) {
						Entity source = ((TNTPrimed) e.getEntity()).getSource();
						if (source instanceof Player && a.isMember((Player) source))
							continue;
					}
					it.remove();
					break;
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onBlockExplode(BlockExplodeEvent e) {
		Iterator<Block> it = e.blockList().iterator();
		while (it.hasNext()) {
			Block block = it.next();
			List<Area> areas = areaManager.getAreasContaining(block.getLocation());
			for (Area a : areas) {
				if (a.isProtectedArea()) {
					it.remove();
					break;
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onEntitySpawn(EntitySpawnEvent e) {
		if (e.getEntity() instanceof Monster) {
			List<Area> areas = areaManager.getAreasContaining(e.getLocation());
			for (Area a : areas) {
				if (a.getProperty("disallow_monsters")) {
					e.setCancelled(true);
					break;
				}
			}
		}
	}

	public void tick() {
		for (Area a : areaManager.getAreas()) {
			if (a.getProperty("disallow_monsters")) {
				Collection<Monster> monsters = Bukkit.getWorld(a.getWorld()).getEntitiesByClass(Monster.class);
				for (Monster m : monsters) {
					if (a.isInArea(m.getLocation())) {
						m.remove();
					}
				}
			}
		}
	}

	public HorseProtectionManager getHorseProtectionManager() {
		return horseProtectionManager;
	}
}
