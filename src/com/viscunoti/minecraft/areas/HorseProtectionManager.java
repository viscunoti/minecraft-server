package com.viscunoti.minecraft.areas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.metadata.FixedMetadataValue;

import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class HorseProtectionManager extends Manager implements Listener {

	public HorseProtectionManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	private void saveOwner(Horse horse, HumanEntity owner) {
		if (horse != null && owner != null) {
			String sql = "INSERT INTO horse_owners (horse, owner) VALUES (?, ?) ON DUPLICATE KEY UPDATE owner = VALUES(owner)";
			database.asyncUpdate(sql, new Object[] { horse.getUniqueId().toString(), owner.getUniqueId().toString() });
			horse.setMetadata("viscunoti.owner", new FixedMetadataValue(plugin, owner.getUniqueId()));
		}
	}

	private UUID getOwner(Horse horse) {
		if (horse != null) {
			if (horse.hasMetadata("viscunoti.owner")) {
				Object value = horse.getMetadata("viscunoti.owner").get(0).value();
				if (value != null)
					return (UUID) value;
				else
					return null;
			} else {
				String sql = "SELECT owner FROM horse_owners WHERE horse = ?";
				PreparedStatement st = null;
				ResultSet rs = null;
				UUID ownerId = null;
				try {
					st = database.prepareStatement(sql);
					st.setString(1, horse.getUniqueId().toString());
					rs = database.query(st);
					if (rs.next()) {
						String owner = rs.getString("owner");
						if (owner != null)
							ownerId = UUID.fromString(rs.getString("owner"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					database.close(rs);
					database.close(st);
				}
				horse.setMetadata("viscunoti.owner", new FixedMetadataValue(plugin, ownerId));
				return ownerId;
			}
		}
		return null;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onEntityInteract(PlayerInteractEntityEvent e) {
		if (e.getRightClicked() instanceof Horse) {
			Horse horse = (Horse) e.getRightClicked();
			UUID owner = getOwner(horse);
			if (horse.isTamed() && owner != null && horse.getInventory().getSaddle() != null
					&& !e.getPlayer().getUniqueId().equals(owner)) {
				e.setCancelled(true);
			} else if (e.getPlayer().getUniqueId().equals(owner)) {
				e.setCancelled(false);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	private void onInventoryClose(InventoryCloseEvent e) {
		if (e.getView().getTopInventory() instanceof HorseInventory) {
			HorseInventory inv = (HorseInventory) e.getView().getTopInventory();
			if (inv.getSaddle() != null && inv.getHolder() instanceof Horse) {
				Horse horse = (Horse) inv.getHolder();
				saveOwner(horse, e.getPlayer());
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		Entity damager = Utils.getLastDamager(e);
		if (damager instanceof Player && e.getEntity() instanceof Horse) {
			Horse horse = (Horse) e.getEntity();
			UUID owner = getOwner(horse);
			if (horse.getInventory().getSaddle() != null && owner != null) {
				Area smallestArea = plugin.getAreaManager().getSmallestAreaContaining(horse.getLocation());
				if (smallestArea != null && smallestArea.isPvpEnabled() || damager.getUniqueId().equals(owner))
					e.setCancelled(false);
				else
					e.setCancelled(true);
			}
		}
	}
}
