package com.viscunoti.minecraft.areas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class AreaManager extends Manager implements Listener {
	private AreaProtectionManager areaProtectionManager;
	private Map<UUID, Area> areas = Maps.newHashMap();
	private Map<String, UUID> areaNames = Maps.newHashMap();
	private Map<UUID, List<Area>> playerAreas = Maps.newHashMap();

	public AreaManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
		areaProtectionManager = new AreaProtectionManager(plugin, database, this);
		Bukkit.getPluginManager().registerEvents(areaProtectionManager, plugin);
		this.tickRate = 20;
	}

	@EventHandler
	private void onPlayerQuit(PlayerQuitEvent e) {
		playerAreas.remove(e.getPlayer().getUniqueId());
	}

	public boolean registerArea(Area area) {
		return registerArea(area, true);
	}

	public boolean registerArea(Area area, boolean save) {
		if (area != null && !areas.containsKey(area.getUniqueId()) && !areaNames.containsKey(area.getName())) {
			area.setAreaManager(this);
			areas.put(area.getUniqueId(), area);
			areaNames.put(area.getName(), area.getUniqueId());
			if (save)
				saveArea(area);
			return true;
		}
		return false;
	}

	public void saveAreas() {
		for (Area a : areas.values())
			saveArea(a);
	}

	public List<Area> getAreas() {
		return ImmutableList.copyOf(areas.values());
	}

	public void reload() {
		areas.clear();
		areaNames.clear();
		String sql = "SELECT * FROM areas";
		ResultSet rs = null;
		PreparedStatement st = null;
		try {
			st = database.prepareStatement(sql);
			rs = database.query(st);
			areas.clear();
			while (rs.next()) {
				try {
					Area area = new Area(this, rs.getString("name"), rs.getString("world"), rs.getDouble("xmin"),
							rs.getDouble("xmax"), rs.getDouble("ymin"), rs.getDouble("ymax"), rs.getDouble("zmin"),
							rs.getDouble("zmax"), UUID.fromString(rs.getString("area_id")));
					if (rs.getString("owner_id") != null)
						area.setOwner(UUID.fromString(rs.getString("owner_id")));
					area.setEnterAnnouncement(rs.getString("enter_announcement"));
					area.setExitAnnouncement(rs.getString("exit_announcement"));
					area.setProtectedArea(rs.getBoolean("protected"));
					area.setPvpEnabled(rs.getBoolean("pvp"));
					sql = "SELECT user_id FROM area_members WHERE area_id = ?";
					PreparedStatement stMembers = null;
					ResultSet rsMembers = null;
					try {
						stMembers = database.prepareStatement(sql);
						stMembers.setString(1, area.getUniqueId().toString());
						rsMembers = database.query(stMembers);
						while (rsMembers.next()) {
							try {
								area.members.add(UUID.fromString(rsMembers.getString("user_id")));
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						database.close(rsMembers);
						database.close(stMembers);
					}
					sql = "SELECT property, `value` FROM area_properties WHERE area_id = ?";
					PreparedStatement stProperties = null;
					ResultSet rsProperties = null;
					try {
						stProperties = database.prepareStatement(sql);
						stProperties.setString(1, area.getUniqueId().toString());
						rsProperties = database.query(stProperties);
						while (rsProperties.next()) {
							try {
								area.properties.put(rsProperties.getString("property"),
										rsProperties.getBoolean("value"));
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						database.close(rsProperties);
						database.close(stProperties);
					}
					registerArea(area, false);
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close(rs);
			database.close(st);
		}
	}

	public List<Area> getAreasContaining(Location loc) {
		if (loc != null) {
			List<Area> ret = Lists.newArrayList();
			for (Area a : areas.values()) {
				if (a.isInArea(loc))
					ret.add(a);
			}
			return ret;
		}
		return null;
	}

	public Area getSmallestAreaContaining(Location loc) {
		if (loc != null) {
			Area smallestArea = null;
			double smallestAreaSize = 0;
			for (Area a : areas.values()) {
				if (a.isInArea(loc)) {
					double areaSize = a.getAreaSizeIgnoreY();
					if (smallestArea == null || areaSize < smallestAreaSize) {
						smallestArea = a;
						smallestAreaSize = areaSize;
					}
				}
			}
			return smallestArea;
		}
		return null;
	}

	public void tick() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			List<Area> oldAreas = playerAreas.get(p.getUniqueId());
			List<Area> newAreas = Lists.newArrayList();
			for (Area a : areas.values()) {
				if (a.isInArea(p.getLocation())) {
					newAreas.add(a);
					if (a.hasEnterAnnouncement() && (oldAreas == null || !oldAreas.contains(a))) {
						p.sendMessage(a.getEnterAnnouncement());
					}
				} else {
					if (oldAreas != null && a.hasExitAnnouncement() && oldAreas.contains(a)) {
						p.sendMessage(a.getExitAnnouncement());
					}
				}
			}
			playerAreas.put(p.getUniqueId(), newAreas);
		}
		areaProtectionManager.tick();
	}

	public void deleteArea(Area area) {
		if (area != null) {
			deleteArea(area.getUniqueId());
		}
	}

	public void deleteArea(UUID areaId) {
		if (areaId != null) {
			Area area = areas.remove(areaId);
			if (area != null) {
				areaNames.remove(area.getName());
				String sql = "DELETE FROM area_members WHERE area_id = ?";
				database.asyncUpdate(sql, new Object[] { areaId.toString() });
				sql = "DELETE FROM areas WHERE area_id = ?";
				database.asyncUpdate(sql, new Object[] { areaId.toString() });
			}
		}
	}

	public Area getArea(UUID areaId) {
		if (areaId != null)
			return areas.get(areaId);
		return null;
	}

	public Area getArea(String name) {
		if (name != null)
			return getArea(areaNames.get(name));
		return null;
	}

	public void saveArea(Area area) {
		String sql = "INSERT INTO areas (area_id, owner_id, name, world, xmin, xmax, ymin, ymax, zmin, zmax, pvp, protected, "
				+ "enter_announcement, exit_announcement) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE owner_id = VALUES(owner_id), "
				+ "name = VALUES(name), world = VALUES(world), xmin = VALUES(xmin), xmax = VALUES(xmax), ymin = VALUES(ymin), ymax = VALUES(ymax), "
				+ "zmin = VALUES(zmin), zmax = VALUES(zmax), pvp = VALUES(pvp), protected = VALUES(protected), enter_announcement = VALUES(enter_announcement), "
				+ "exit_announcement = VALUES(exit_announcement)";
		Object[] params = new Object[] { area.getUniqueId().toString(), null, area.getName(), area.getWorld(),
				area.getXmin(), area.getXmax(), area.getYmin(), area.getYmax(), area.getZmin(), area.getZmax(),
				area.isPvpEnabled(), area.isProtectedArea(), area.getEnterAnnouncement(), area.getExitAnnouncement() };
		if (area.getOwner() != null)
			params[1] = area.getOwner().toString();
		database.asyncUpdate(sql, params);
	}

	public boolean addMember(Area area, UUID playerId) {
		String sql = "INSERT INTO area_members (area_id, user_id) VALUES (?, ?)";
		database.asyncUpdate(sql, new Object[] { area.getUniqueId().toString(), playerId.toString() });
		if (area.members.contains(playerId))
			return false;
		area.members.add(playerId);
		return true;
	}

	public boolean removeMember(Area area, UUID playerId) {
		String sql = "DELETE FROM area_members WHERE area_id = ? AND user_id = ?";
		database.asyncUpdate(sql, new Object[] { area.getUniqueId().toString(), playerId.toString() });
		return area.members.remove(playerId);
	}

	public void addProperty(Area area, String property, boolean value) {
		String sql = "INSERT INTO area_properties (area_id, property, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value = VALUES(value)";
		database.asyncUpdate(sql, new Object[] { area.getUniqueId().toString(), property, value });
		area.properties.put(property, value);
	}

	public void removeProperty(Area area, String property) {
		String sql = "DELETE FROM area_properties WHERE area_id = ? AND property = ?";
		database.asyncUpdate(sql, new Object[] { area.getUniqueId().toString(), property });
		area.properties.remove(property);
	}

	public void rename(Area area, String name) {
		areaNames.remove(area.getName());
		areaNames.put(name, area.getUniqueId());
		area.name = name;
	}

	public AreaProtectionManager getAreaProtectionManager() {
		return areaProtectionManager;
	}
}
