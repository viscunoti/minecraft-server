package com.viscunoti.minecraft.combatlogging;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;

public class DamageLogMetadata implements MetadataValue {

	private Cache<UUID, Double> damagers;
	private Plugin plugin;
	private double damageTaken = 0;

	public DamageLogMetadata(Plugin plugin) {
		this.plugin = plugin;
		damagers = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).build();
	}

	public void addDamageFromPlayer(UUID player, double damage) {
		Double currentDamage = damagers.getIfPresent(player);
		damagers.put(player, currentDamage == null ? damage : currentDamage + damage);
	}

	public void addDamageTaken(double damage) {
		damageTaken += damage;
	}

	public Map<UUID, Double> getDamagers() {
		damagers.cleanUp();
		return ImmutableMap.copyOf(damagers.asMap());
	}

	public boolean asBoolean() {

		return false;
	}

	public byte asByte() {
		return 0;
	}

	public double asDouble() {
		return 0;
	}

	public float asFloat() {
		return 0;
	}

	public int asInt() {
		return 0;
	}

	public long asLong() {
		return 0;
	}

	public short asShort() {
		return 0;
	}

	public String asString() {
		return null;
	}

	public Plugin getOwningPlugin() {
		return plugin;
	}

	public void invalidate() {
		damagers = null;
	}

	public Object value() {
		return null;
	}

	public double getDamage() {
		return damageTaken;
	}

}
