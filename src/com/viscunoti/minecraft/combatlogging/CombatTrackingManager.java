package com.viscunoti.minecraft.combatlogging;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.MetadataValue;

import com.google.common.collect.Maps;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class CombatTrackingManager extends Manager implements Listener {
	public CombatTrackingManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	@EventHandler
	private void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof LivingEntity) {
			Entity damager = Utils.getLastDamager(e);
			if (damager instanceof Player) {
				Entity entity = e.getEntity();
				DamageLogMetadata metadata = getMetadataDamagers(entity);
				metadata.addDamageFromPlayer(damager.getUniqueId(), e.getDamage());
			}
		}
	}

	@EventHandler
	private void onEntityDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof LivingEntity) {
			DamageLogMetadata metadata = getMetadataDamagers(e.getEntity());
			metadata.addDamageTaken(e.getDamage());
		}
	}

	public Map<UUID, Double> getDamagers(Entity entity) {
		DamageLogMetadata metadata = getMetadataDamagers(entity);
		if (metadata != null)
			return metadata.getDamagers();
		return Maps.newHashMapWithExpectedSize(0);
	}

	public double getDamageTaken(Entity entity) {
		DamageLogMetadata metadata = getMetadataDamagers(entity);
		if (metadata != null)
			return metadata.getDamage();
		return 0;
	}

	public DamageLogMetadata getMetadataDamagers(Entity entity) {
		if (entity != null) {
			if (!entity.hasMetadata("viscunoti.damagers"))
				entity.setMetadata("viscunoti.damagers", new DamageLogMetadata(plugin));
			List<MetadataValue> metadata = entity.getMetadata("viscunoti.damagers");
			if (metadata.size() > 0 && metadata.get(0) instanceof DamageLogMetadata) {
				return (DamageLogMetadata) metadata.get(0);
			}
		}
		return null;
	}
}
