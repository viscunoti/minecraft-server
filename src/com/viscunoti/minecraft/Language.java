package com.viscunoti.minecraft;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Language {
	public static String formatMoney(double money) {
		NumberFormat format = DecimalFormat.getInstance();

		format.setRoundingMode(RoundingMode.FLOOR);
		format.setMinimumFractionDigits(2);
		format.setMaximumFractionDigits(2);

		return format.format(money) + " gold";
	}

	public static String formatMoney(BigDecimal money) {
		NumberFormat format = DecimalFormat.getInstance();

		format.setRoundingMode(RoundingMode.HALF_UP);
		format.setMinimumFractionDigits(2);
		format.setMaximumFractionDigits(2);

		return format.format(money) + " gold";
	}

	public static String formatDuration(long duration) {
		duration /= 1000;

		long s = duration % 60;
		long m = (duration / 60) % 60;
		long h = (duration / 3600) % 24;
		long d = (duration / 86400) % 365;
		long y = (duration / 31536000);

		StringBuilder sb = new StringBuilder();
		if (y != 0) {
			sb.append(y);
			if (y != 1)
				sb.append(" years, ");
			else
				sb.append(" year, ");
		}
		if (d != 0) {
			sb.append(d);
			if (d != 1)
				sb.append(" days, ");
			else
				sb.append(" day, ");
		}
		if (h != 0) {
			sb.append(h);
			if (h != 1)
				sb.append(" hours, ");
			else
				sb.append(" hour, ");
		}
		if (m != 0) {
			sb.append(m);
			if (m != 1)
				sb.append(" minutes, ");
			else
				sb.append(" minute, ");
		}
		if (s != 0) {
			sb.append(s);
			if (s != 1)
				sb.append(" seconds, ");
			else
				sb.append(" second, ");
		}

		if (sb.length() != 0)
			sb.setLength(sb.length() - 2);
		return sb.toString();
	}

	public static String translate(String string) {
		return translate(string, false);
	}

	public static String translate(String string, boolean capitalize) {
		StringBuilder sb = new StringBuilder(string.length());
		boolean capitalizeNext = true;
		for (char c : string.toCharArray()) {
			if (c == ' ' || c == '_') {
				sb.append(' ');
				capitalizeNext = true;
			} else {
				if (capitalizeNext && capitalize) {
					sb.append(Character.toUpperCase(c));
					capitalizeNext = false;
				} else {
					sb.append(Character.toLowerCase(c));
				}
			}
		}
		return sb.toString();
	}
}
