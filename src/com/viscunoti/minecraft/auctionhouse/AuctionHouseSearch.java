package com.viscunoti.minecraft.auctionhouse;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

public class AuctionHouseSearch {
	private String item = null;
	private UUID sellerId = null;

	public AuctionHouseSearch() {

	}

	public AuctionHouseSearch setItemSearch(String itemSearch) {
		this.item = itemSearch;
		return this;
	}

	public AuctionHouseSearch setSellerSearch(OfflinePlayer player) {
		if (player != null)
			setSellerSearch(player.getUniqueId());
		else
			this.sellerId = null;
		return this;
	}

	public AuctionHouseSearch setSellerSearch(UUID sellerId) {
		this.sellerId = sellerId;
		return this;
	}

	public boolean isSellerSearch() {
		return sellerId != null;
	}

	public boolean isItemSearch() {
		return item != null && !item.isEmpty();
	}

	public UUID getSellerSearch() {
		return sellerId;
	}

	public String getItemSearch() {
		return item;
	}
}
