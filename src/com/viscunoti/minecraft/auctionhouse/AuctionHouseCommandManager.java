package com.viscunoti.minecraft.auctionhouse;

import java.math.BigDecimal;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.base.Joiner;
import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.Language;

public class AuctionHouseCommandManager extends CommandManager {
	private AuctionHouseManager auctionHouseManager;

	public AuctionHouseCommandManager(AuctionHouseManager auctionHouseManager) {
		this.auctionHouseManager = auctionHouseManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (args.length >= 1 && args[0].equalsIgnoreCase("search")) {
				if (auctionHouseManager.isAuctioneerNearby(p.getLocation())) {
					String search = null;
					if (args.length > 1)
						search = Joiner.on('_').join(Arrays.copyOfRange(args, 1, args.length));
					auctionHouseManager.search(p, new AuctionHouseSearch().setItemSearch(search), 1);
				} else
					p.sendMessage(ChatColor.GRAY + "There is no auctioneer nearby.");
				return true;
			} else if (args.length == 2 && (args[0].equalsIgnoreCase("post") || args[0].equalsIgnoreCase("sell"))) {
				if (auctionHouseManager.isAuctioneerNearby(p.getLocation())) {
					try {
						ItemStack item = p.getItemInHand();
						if (item != null && !item.getType().equals(Material.AIR) && item.getItemMeta() != null) {
							BigDecimal price = new BigDecimal(args[1]);
							auctionHouseManager.addItem(p, item, price);
							p.setItemInHand(null);
							p.sendMessage(ChatColor.GOLD + "Posted " + Language.translate(item.getType().toString()) + " for "
									+ Language.formatMoney(price) + ".");
						} else
							p.sendMessage(ChatColor.GRAY + "You do not have an item in your hand.");
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else
					p.sendMessage(ChatColor.GRAY + "There is no auctioneer nearby.");
				return true;
			} else if (args.length >= 1 && args[0].equalsIgnoreCase("cancel")) {
				if (auctionHouseManager.isAuctioneerNearby(p.getLocation())) {
					String search = null;
					if (args.length > 1)
						search = Joiner.on('_').join(Arrays.copyOfRange(args, 1, args.length));
					auctionHouseManager.search(p, new AuctionHouseSearch().setSellerSearch(p).setItemSearch(search), 1);
				} else
					p.sendMessage(ChatColor.GRAY + "There is no auctioneer nearby.");
				return true;
			}
		}
		return false;
	}
}
