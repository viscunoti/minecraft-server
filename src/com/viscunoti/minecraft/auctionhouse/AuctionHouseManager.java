package com.viscunoti.minecraft.auctionhouse;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.citizens.AuctioneerTrait;
import com.viscunoti.minecraft.database.Database;
import com.viscunoti.minecraft.database.QueryRunnable;
import com.viscunoti.minecraft.mail.MailManager;
import com.viscunoti.minecraft.menus.Menu;
import com.viscunoti.minecraft.menus.MenuItem;
import com.viscunoti.minecraft.menus.MenuItemRunnable;
import com.viscunoti.minecraft.menus.MenuManager;
import com.viscunoti.minecraft.money.MoneyManager;
import com.viscunoti.minecraft.money.MoneySource;

public class AuctionHouseManager extends Manager {
	private static final int MENU_SIZE = 54;
	private BigDecimal auctionHouseCut;
	private MenuManager menuManager;
	private MoneyManager moneyManager;
	private MailManager mailManager;

	public AuctionHouseManager(ViscunotiPlugin plugin, Database database, MenuManager menuManager,
			MoneyManager moneyManager, MailManager mailManager) {
		super(plugin, database);
		this.menuManager = menuManager;
		this.moneyManager = moneyManager;
		this.mailManager = mailManager;
	}

	public void reload() {
		auctionHouseCut = new BigDecimal(plugin.getConfig().getString("Auction House.Cut Multiplier"));
	}

	public void addItem(OfflinePlayer seller, ItemStack item, BigDecimal price) {
		if (seller != null && item != null && price != null && price.compareTo(BigDecimal.ZERO) == 1) {
			String sql = "INSERT INTO auctions (user_id, item, price_per_item, price, itemstack, itemstack_json) VALUES (?, ?, ?, ?, ?, ?)";
			Gson gson = Utils.getIgnoreDuplicateGson();
			database.asyncUpdate(
					sql,
					new Object[] {
							seller.getUniqueId().toString(),
							item.getType().toString(),
							price.divide(BigDecimal.valueOf(item.getAmount() > 1 ? item.getAmount() : 1), 2,
									BigDecimal.ROUND_HALF_UP), price, Utils.serializeBukkit(item),
							gson.toJson(item.serialize()) });
		}
	}

	public void search(Player player, AuctionHouseSearch search, int page) {
		if (player == null)
			return;
		player.setMetadata("viscunoti.auction.search", new FixedMetadataValue(plugin, search));
		player.setMetadata("viscunoti.auction.page", new FixedMetadataValue(plugin, page));
		StringBuilder sql = new StringBuilder("SELECT * FROM auctions WHERE active = 1");
		List<Object> params = Lists.newArrayList();
		if (search.isItemSearch()) {
			sql.append(" AND item LIKE ?");
			params.add(search.getItemSearch().replace("_", "\\_"));
		}
		if (search.isSellerSearch()) {
			sql.append(" AND user_id = ?");
			params.add(search.getSellerSearch().toString());
		}
		sql.append(" ORDER BY item, price_per_item, price ASC LIMIT ?, ?");
		params.add((page - 1) * MENU_SIZE);
		params.add(MENU_SIZE - 2);
		database.asyncQuery(sql.toString(), params.toArray(), new QueryRunnable() {
			public void run() {
				String title = "", titleSearch = search.getItemSearch(), titlePage = " (Page " + page + ")";
				if (search.isSellerSearch()) {
					OfflinePlayer seller = Bukkit.getOfflinePlayer(search.getSellerSearch());
					if (seller != null)
						title = seller.getName() + ": ";
				}
				if (search.isItemSearch()) {
					int titleLength = title.length() + titleSearch.length() + titlePage.length();
					if (titleLength > 32) {
						int end = titleSearch.length() - (titleLength - 32) - 3;
						if (end < 0)
							end = 0;
						titleSearch = titleSearch.substring(0, end) + "...";
					}
					title += titleSearch + titlePage;
				}
				if (title.isEmpty())
					title = "Auction House";
				if (title.length() > 32)
					title = title.substring(0, 31);
				Menu menu = new Menu(menuManager, title, MENU_SIZE);
				int index = 0;
				try {
					while (results.next()) {
						try {
							if (index == 45)
								index++;
							if (index == 53)
								break;
							ItemStack item = (ItemStack) Utils.deserializeBukkit(results.getBlob("itemstack")
									.getBinaryStream());
							if (item == null || item.getType().equals(Material.AIR) || item.getItemMeta() == null) {
								remove(results.getInt("id"));
								continue;
							}
							ItemMeta meta = item.getItemMeta();
							meta.setDisplayName(null);
							List<String> lore = Lists.newArrayList(
									ChatColor.GOLD + Language.formatMoney(results.getBigDecimal("price")),
									ChatColor.GOLD + Language.formatMoney(results.getBigDecimal("price_per_item"))
											+ "/item");
							if (results.getString("user_id").equals(player.getUniqueId().toString()))
								lore.add(ChatColor.RESET + "You are selling this item.");
							meta.setLore(lore);
							item.setItemMeta(meta);
							int auctionId = results.getInt("id");
							menu.setItem(index, new MenuItem(item, new MenuItemRunnable() {
								public void run(Player player, Menu menu) {
									purchase(player, auctionId);
								}
							}));
							index++;
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (index == MENU_SIZE)
							break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				int numItems = index;
				menu.setItem(
						45,
						new MenuItem(Utils.setName(new ItemStack(Material.BEDROCK), ChatColor.RESET + "Page "
								+ (page - 1)), new MenuItemRunnable() {
							public void run(Player player, Menu menu) {
								if (page > 1) {
									player.setMetadata("viscunoti.auction.page", new FixedMetadataValue(plugin,
											page - 1));
									updateSearch(player);
								}
							}
						}));
				menu.setItem(
						53,
						new MenuItem(Utils.setName(new ItemStack(Material.BEDROCK), ChatColor.RESET + "Page "
								+ (page + 1)), new MenuItemRunnable() {
							public void run(Player player, Menu menu) {
								if (numItems > 0) {
									player.setMetadata("viscunoti.auction.page", new FixedMetadataValue(plugin,
											page + 1));
									updateSearch(player);
								}
							}
						}));
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						menu.open(player);
					}
				});
			}
		});
	}

	public void purchase(Player player, int auctionId) {
		String sql = "SELECT * FROM auctions WHERE id = ? AND active = 1";
		database.asyncQuery(sql, new Object[] { auctionId }, new QueryRunnable() {
			public void run() {
				try {
					if (!results.next()) {
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								player.sendMessage(ChatColor.GRAY + "Auction not found.");
							}
						});
						return;
					}
					UUID sellerId = UUID.fromString(results.getString("user_id"));
					ItemStack item = (ItemStack) Utils.deserializeBukkit(results.getBinaryStream("itemstack"));
					BigDecimal price = results.getBigDecimal("price");
					if (!sellerId.equals(player.getUniqueId())) {
						if (moneyManager.subtractMoney(player, price, MoneySource.AUCTION_HOUSE_PURCHASE)) {
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
									mailManager.giveItem(player, item);
									player.sendMessage(ChatColor.GOLD + "You purchased "
											+ Language.translate(item.getType().toString()) + " (" + item.getAmount()
											+ ") for " + Language.formatMoney(price) + ".");
									remove(auctionId);
									moneyManager.addMoney(sellerId, price, MoneySource.AUCTION_HOUSE_SALE);
									Player seller = Bukkit.getPlayer(sellerId);
									if (seller != null)
										seller.sendMessage(ChatColor.GOLD + "Your "
												+ Language.translate(item.getType().toString())
												+ " has sold and you received "
												+ Language.formatMoney(price.multiply(auctionHouseCut)) + ".");
									updateSearch(player);
								}
							});
						} else {
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public void run() {
									player.sendMessage(ChatColor.GRAY + "You need an additional "
											+ Language.formatMoney(price.subtract(moneyManager.getMoney(player)))
											+ " to afford that.");
								}
							});
						}
					} else {
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								remove(auctionId);
								mailManager.giveItem(player, item);
								player.sendMessage("Cancelled auction of "
										+ Language.translate(item.getType().toString()) + " (" + item.getAmount()
										+ ") for " + Language.formatMoney(price) + ".");
								updateSearch(player);
							}
						});
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void updateSearch(Player player) {
		if (player != null) {
			List<MetadataValue> search = player.getMetadata("viscunoti.auction.search");
			List<MetadataValue> page = player.getMetadata("viscunoti.auction.page");
			if (!search.isEmpty() && !page.isEmpty()) {
				if (search.get(0).value() instanceof AuctionHouseSearch) {
					search(player, (AuctionHouseSearch) search.get(0).value(), page.get(0).asInt());
				}
			}
		}
	}

	public void remove(int auctionId) {
		String sql = "UPDATE auctions SET active = 0 WHERE id = ?";
		database.asyncUpdate(sql, new Object[] { auctionId });
	}

	public boolean isAuctioneerNearby(Location loc) {
		if (loc != null) {
			double radius = plugin.getConfig().getDouble("Auction House.Auctioneer Radius");
			double radiusSquared = radius * radius;
			Collection<Entity> entities = loc.getWorld().getNearbyEntities(loc, radius, radius, radius);
			NPCRegistry npcRegistry = CitizensAPI.getNPCRegistry();
			for (Entity e : entities) {
				if (loc.distanceSquared(e.getLocation()) <= radiusSquared && npcRegistry.isNPC(e)) {
					NPC npc = npcRegistry.getNPC(e);
					if (npc.hasTrait(AuctioneerTrait.class))
						return true;
				}
			}
		}
		return false;
	}
}
