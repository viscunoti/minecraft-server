package com.viscunoti.minecraft.mail;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;
import com.viscunoti.minecraft.database.UpdateRunnable;

public class MailManager extends Manager implements Listener {

	public MailManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	@EventHandler
	private void onPlayerJoin(PlayerJoinEvent e) {
		Mail m = getNextMail(e.getPlayer());
		if (m != null)
			e.getPlayer().sendMessage("You have new mail!  Type /mail to open the next message.");
	}

	public Mail getNextMail(OfflinePlayer player) {
		String sql = "SELECT id, to_id, `from`, message, itemstack, created_at FROM mail WHERE to_id = ? AND opened_at IS NULL ORDER BY created_at LIMIT 1";
		PreparedStatement st = database.prepareStatement(sql);
		Mail mail = null;
		try {
			st.setString(1, player.getUniqueId().toString());
			ResultSet rs = database.query(st);
			if (rs.next()) {
				Blob itemBlob = rs.getBlob("itemstack");
				ItemStack item = null;
				if (itemBlob != null) {
					Object obj = Utils.deserializeBukkit(itemBlob.getBinaryStream());
					if (obj instanceof ItemStack)
						item = (ItemStack) obj;
				}
				mail = new Mail(rs.getString("to_id"), rs.getString("from"), rs.getString("message"), item, rs
						.getTimestamp("created_at").getTime());
				mail.id = rs.getInt("id");
			}
			database.close(rs);
		} catch (Exception e) {
			e.printStackTrace();
			database.close(st);
		}
		return mail;
	}

	public void openMail(Player player, Mail mail) {
		if (player != null && mail != null && mail.id != -1) {
			String sql = "UPDATE mail SET opened_at = CURRENT_TIMESTAMP WHERE id = ?";
			database.asyncUpdate(sql, new Object[] { mail.id }, new UpdateRunnable() {
				public void run() {
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							player.sendMessage("----------");
							player.sendMessage("Mail From: " + mail.getFrom());
							player.sendMessage("----------");
							player.sendMessage(mail.getMessage());
							if (mail.getItem() != null) {
								player.sendMessage("----------");
								player.sendMessage("The message contained a "
										+ Language.translate(mail.getItem().getType().toString()) + ".");
							}
							player.sendMessage("----------");
							Utils.giveItems(player, mail.createBook(), mail.getItem());
							int mailRemaining = getNumMails(player);
							if (mailRemaining > 0)
								player.sendMessage("You have " + mailRemaining + " messages remaining.");
						}
					});
				}
			});
		}
	}

	public int getNumMails(OfflinePlayer player) {
		int ret = -1;
		if (player != null) {
			PreparedStatement st = database
					.prepareStatement("SELECT COUNT(1) as messages FROM mail WHERE to_id = ? AND opened_at IS NULL LIMIT 50");
			try {
				st.setString(1, player.getUniqueId().toString());
				ResultSet rs = database.query(st);
				if (rs.next()) {
					ret = rs.getInt("messages");
				}
				database.close(rs);
			} catch (SQLException e) {
				e.printStackTrace();
				database.close(st);
			}
		}
		return ret;
	}

	public void addMail(OfflinePlayer to, Mail mail) {
		String sql = "INSERT INTO mail (to_id, `from`, message, itemstack, itemstack_json) VALUES (?, ?, ?, ?, ?)";
		ItemStack itemstack = null;
		String itemstackJson = null;
		if (mail.getItem() != null && !mail.getItem().getType().equals(Material.AIR)) {
			itemstack = mail.getItem();
			Gson gson = Utils.getIgnoreDuplicateGson();
			itemstackJson = gson.toJson(mail.getItem().serialize());
		}

		database.asyncUpdate(sql, new Object[] { to.getUniqueId().toString(), mail.getFrom(), mail.getMessage(),
				itemstack != null ? new ByteArrayInputStream(Utils.serializeBukkit(itemstack)) : null, itemstackJson });

		if (to.isOnline()) {
			((Player) to).sendMessage("You have recieved mail from " + mail.getFrom()
					+ ".  Type /mail to open the next message!");
		}
	}

	public void giveItem(Player player, ItemStack item) {
		if (player != null && item != null) {
			Map<Integer, ItemStack> overflow = player.getInventory().addItem(item);
			for (ItemStack i : overflow.values()) {
				Mail mail = new Mail("Postmaster", "I saw this fall out of your pocket. ", i);
				addMail(player, mail);
			}
		}
	}
}
