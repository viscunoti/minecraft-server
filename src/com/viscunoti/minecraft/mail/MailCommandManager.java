package com.viscunoti.minecraft.mail;

import java.math.BigDecimal;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.money.MoneyManager;
import com.viscunoti.minecraft.money.MoneySource;

public class MailCommandManager extends CommandManager {
	protected ViscunotiPlugin plugin;
	protected MailManager mailManager;
	protected MoneyManager moneyManager;

	public MailCommandManager(ViscunotiPlugin plugin, MailManager mailManager, MoneyManager moneyManager) {
		this.plugin = plugin;
		this.mailManager = mailManager;
		this.moneyManager = moneyManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (args.length == 0) {
				Mail m = mailManager.getNextMail(p);
				if (m == null)
					p.sendMessage("Your inbox is empty.");
				else
					mailManager.openMail(p, m);
				return true;
			} else if (args.length >= 1) {
				ItemStack item = null;
				int index = 0;
				if (args[0].equals("-i")) {
					item = p.getItemInHand();
					index++;
				}
				OfflinePlayer target = Utils.getOfflinePlayer(args[index]);
				index++;
				FileConfiguration config = plugin.getConfig();
				if (target == null) {
					p.sendMessage(ChatColor.RED + "The player could not be found.");
					return false;
				}
				if (!config.getBoolean("Allow send to self") && target.getName().equalsIgnoreCase(p.getName())) {
					p.sendMessage(ChatColor.RED + "You can not send mail to yourself.");
					return true;
				}
				BigDecimal sendCost = new BigDecimal(plugin.getConfig().getString("Mail.Send Cost"));
				if (moneyManager.getMoney(p).compareTo(sendCost) == -1) {
					p.sendMessage(ChatColor.GRAY + "You don't have the required " + Language.formatMoney(sendCost)
							+ " to send mail.");
					return true;
				}
				moneyManager.addMoney(p, sendCost.negate(), MoneySource.SEND_MAIL);
				Mail m = new Mail(p.getName(), Utils.joinArgs(args, index), item);
				if (item != null)
					p.setItemInHand(null);
				mailManager.addMail(target, m);
				m.setSent();
				Utils.giveItems(p, m.createBook());
				p.sendMessage(ChatColor.GOLD + "You gave the post office " + Language.formatMoney(sendCost) + ".");
				p.sendMessage("The message was sent and you have been given a copy.");
				return true;
			}
		}
		return false;
	}

}
