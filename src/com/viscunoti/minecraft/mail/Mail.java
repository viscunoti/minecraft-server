package com.viscunoti.minecraft.mail;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import com.google.common.base.Splitter;
import com.viscunoti.minecraft.Utils;

public class Mail {
	private String to, from, message;
	private ItemStack item;
	private long sent, opened;
	protected int id = -1;

	public Mail(String from, String message, ItemStack item) {
		this(null, from, message, item, -1, -1);
	}

	protected Mail(String to, String from, String message, ItemStack item, long sent) {
		this(to, from, message, item, sent, -1);
	}

	protected Mail(String to, String from, String message, ItemStack item, long sent, long opened) {
		this.to = to;
		this.from = from;
		this.message = message;
		this.item = item;
		this.sent = sent;
		this.opened = opened;
	}

	public ItemStack createBook() {
		if (sent == -1)
			return null;
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta bm = (BookMeta) book.getItemMeta();
		bm.setAuthor(from);
		String bookText = "From: " + from + "\n\n" + message + "\n\n\nSent at " + Utils.formatTimestamp(sent)
				+ "\n\nOpened at " + Utils.formatTimestamp(opened == -1 ? System.currentTimeMillis() : opened);
		List<String> pages = Splitter.fixedLength(256).splitToList(bookText);
		bm.setPages(pages);
		if (message.length() > 12)
			bm.setTitle(message.substring(0, 12) + "...");
		else
			bm.setTitle(message);
		book.setItemMeta(bm);
		return book;
	}

	public String getTo() {
		return to;
	}

	public String getFrom() {
		return from;
	}

	public String getMessage() {
		return message;
	}

	public ItemStack getItem() {
		return item;
	}
	
	public void setSent(){
		this.sent = System.currentTimeMillis();
	}
}
