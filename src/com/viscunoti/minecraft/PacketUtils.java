package com.viscunoti.minecraft;

import net.minecraft.server.v1_8_R3.PacketPlayOutExperience;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PacketUtils {

	public static void updateExp(Player player) {
		PacketPlayOutExperience packet = new PacketPlayOutExperience(player.getExp(), player.getExpToLevel(), player.getLevel());
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}

}
