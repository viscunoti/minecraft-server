package com.viscunoti.minecraft;

import java.lang.reflect.Field;
import java.util.Set;

import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Sets;

// The things that go on in here aren't good, they just make everything outside of here easier.
public class RecursiveManagerReloader {
	private JavaPlugin plugin;
	private Class<?> startPoint;
	private Set<Manager> reloaded;

	public RecursiveManagerReloader(JavaPlugin plugin, Class<?> clazz) {
		this.plugin = plugin;
		this.startPoint = clazz;
	}

	public void reloadManagers(Object obj) {
		Field[] fields = startPoint.getDeclaredFields();
		reloaded = Sets.newHashSet();
		for (Field f : fields) {
			try {
				boolean accessible = f.isAccessible();
				f.setAccessible(true);
				if (Manager.class.isAssignableFrom(f.getType())) {
					Manager manager = (Manager) f.get(obj);
					try {
						reloadManager(manager);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				f.setAccessible(accessible);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		reloaded = null;
		plugin.getLogger().info("Reloaded all managers");
	}

	private void reloadManager(Manager manager) {
		Field[] fields = manager.getClass().getDeclaredFields();
		try {
			plugin.getLogger().info("Reloading " + manager.getClass().getSimpleName());
			reloaded.add(manager);
			manager.reload();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (Field f : fields) {
			try {
				boolean accessible = f.isAccessible();
				f.setAccessible(true);
				if (!reloaded.contains(f.get(manager))) {
					if (Manager.class.isAssignableFrom(f.getType())) {
						Manager child = (Manager) f.get(manager);
						reloadManager(child);
					}
				}
				f.setAccessible(accessible);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
