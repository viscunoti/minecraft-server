package com.viscunoti.minecraft.citizens;

import java.math.BigDecimal;

import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.viscunoti.minecraft.Language;

public class VendorTrait extends ViscunotiTrait {
	public VendorTrait() {
		super("vendor");
	}

	@EventHandler
	private void onLeftClick(NPCLeftClickEvent e) {
		if (e.getNPC() == this.getNPC()) {
			VendorManager vendorManager = plugin.getCitizensManager().getVendorManager();
			vendorManager.openSellInventory(e.getClicker(), this.getNPC().getName());

		}
	}

	@EventHandler
	private void onRightClick(NPCRightClickEvent e) {
		if (e.getNPC() == this.getNPC()) {
			VendorManager vendorManager = plugin.getCitizensManager().getVendorManager();
			Player p = e.getClicker();
			BigDecimal sellPrice = vendorManager.getSellPrice(p.getItemInHand());
			if (sellPrice != null) {
				whisper(p,
						"I will pay " + Language.formatMoney(sellPrice) + " for "
								+ Language.translate(p.getItemInHand().getType().toString()) + ".");
			}
		}
	}
}
