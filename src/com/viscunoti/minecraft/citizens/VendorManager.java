package com.viscunoti.minecraft.citizens;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;
import com.viscunoti.minecraft.money.MoneySource;

public class VendorManager extends Manager implements Listener {
	private Map<UUID, String> players = Maps.newHashMap();
	private FileConfiguration sellPrices;

	public VendorManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	public void openSellInventory(Player player, String vendorName) {
		Inventory inventory = Bukkit.createInventory(null, 27, "Sell");
		player.openInventory(inventory);
		players.put(player.getUniqueId(), vendorName);
	}

	@EventHandler
	private void onInventoryClose(InventoryCloseEvent e) {
		if (e.getPlayer() instanceof Player && players.containsKey(e.getPlayer().getUniqueId())) {
			Player p = (Player) e.getPlayer();
			String npcName = players.get(p.getUniqueId());
			players.remove(p.getUniqueId());
			Inventory sell = e.getView().getTopInventory();
			BigDecimal total = BigDecimal.ZERO;
			for (ItemStack item : sell.getContents()) {
				if (item != null && !item.getType().equals(Material.AIR)) {
					try {
						BigDecimal sellPrice = getSellPrice(item);
						if (sellPrice == null) {
							plugin.getMailManager().giveItem(p, item);
							p.sendMessage("<" + npcName + "> I do not want to buy "
									+ Language.translate(item.getType().toString()) + ".");
						} else {
							plugin.getMoneyManager().addMoney(p.getUniqueId(), sellPrice, MoneySource.VENDOR);
							total = total.add(sellPrice);
							String itemName = item.getType().toString();
							if (item.getAmount() > 1)
								itemName += " (" + item.getAmount() + ")";
							p.sendMessage(ChatColor.GOLD + "Sold " + Language.translate(itemName) + " for "
									+ Language.formatMoney(sellPrice) + ".");
							logVendoredItem(npcName, p, item, sellPrice);
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
			if (total != BigDecimal.ZERO) {
				p.sendMessage(ChatColor.GOLD + "Total: " + Language.formatMoney(total));
				p.sendMessage("<" + npcName + "> Thanks, have a good one!");
			}
		}
	}

	public BigDecimal getSellPrice(ItemStack item) {
		try {
			if (item != null) {
				String priceString = sellPrices.getString(item.getType().toString());
				if (priceString != null)
					return new BigDecimal(priceString).multiply(BigDecimal.valueOf(item.getAmount()));
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return null;
	}

	private void logVendoredItem(String vendorName, Player player, ItemStack item, BigDecimal price) {
		String sql = "INSERT INTO vendored_items (vendor, player, item, item_json, price) VALUES (?, ?, ?, ?, ?)";
		Gson gson = Utils.getIgnoreDuplicateGson();
		try {
			database.asyncUpdate(
					sql,
					new Object[] { vendorName, player.getUniqueId().toString(), Utils.serializeBukkit(item),
							gson.toJson(item.serialize()), price });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reload() {
		sellPrices = plugin.getCustomConfig("item_vendor_values.yml", false);
	}
}
