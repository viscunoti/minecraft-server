package com.viscunoti.minecraft.citizens;

import java.util.List;

import net.citizensnpcs.api.event.NPCRightClickEvent;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import com.viscunoti.minecraft.ViscunotiPlugin;

public class AuctioneerTrait extends ViscunotiTrait {
	private ViscunotiPlugin plugin;

	public AuctioneerTrait() {
		super("auctioneer");
		plugin = (ViscunotiPlugin) Bukkit.getPluginManager().getPlugin("ViscunotiMC");
	}

	@EventHandler
	private void onRightClick(NPCRightClickEvent e) {
		if (e.getNPC() == this.getNPC()) {
			List<String> messages = plugin.getConfig().getStringList("Auction House.Help Messages");
			e.getClicker().sendMessage(messages.toArray(new String[messages.size()]));
		}
	}
}
