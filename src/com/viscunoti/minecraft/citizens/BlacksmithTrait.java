package com.viscunoti.minecraft.citizens;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.viscunoti.minecraft.Language;

public class BlacksmithTrait extends ViscunotiTrait {
	private Cache<UUID, ItemStack> confirmation;

	public BlacksmithTrait() {
		super("blacksmith");
		confirmation = CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.SECONDS).build();
	}

	@EventHandler
	public void onRightClick(NPCRightClickEvent e) {
		if (e.getNPC() == this.getNPC()) {
			BlacksmithManager blacksmithManager = plugin.getCitizensManager().getBlacksmithManager();
			ItemStack item = e.getClicker().getItemInHand();
			double cost = blacksmithManager.getRepairCost(item);
			if (cost > 0) {
				whisper(e.getClicker(), "It will cost you " + Language.formatMoney(cost) + " to repair that.");
			}
		}
	}

	@EventHandler
	public void onLeftClick(NPCLeftClickEvent e) {
		if (e.getNPC() == this.getNPC()) {
			Player p = e.getClicker();
			BlacksmithManager blacksmithManager = plugin.getCitizensManager().getBlacksmithManager();
			ItemStack item = p.getItemInHand();
			double repairCost = blacksmithManager.getRepairCost(item);
			ItemStack cachedItem = confirmation.getIfPresent(p.getUniqueId());
			if (!item.equals(cachedItem)) {
				confirmation.put(p.getUniqueId(), item);
				whisper(p, "Would like to repair your " + Language.translate(item.getType().toString()) + " for "
						+ Language.formatMoney(repairCost) + "?");
			} else {
				confirmation.invalidate(p.getUniqueId());
				if (blacksmithManager.repair(p, item))
					p.sendMessage(ChatColor.GOLD + "Repaired " + Language.translate(item.getType().toString())
							+ " for " + Language.formatMoney(repairCost) + ".");
				else if (repairCost > 0)
					p.sendMessage(ChatColor.GRAY + "You do not have "
							+ Language.formatMoney(blacksmithManager.getRepairCost(item)) + " to repair your "
							+ Language.translate(item.getType().toString()) + ".");
			}
		}
	}
}
