package com.viscunoti.minecraft.citizens;

import java.math.BigDecimal;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;
import com.viscunoti.minecraft.money.MoneySource;

public class BlacksmithManager extends Manager {
	private FileConfiguration config;

	public BlacksmithManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	public void reload() {
		config = plugin.getCustomConfig("blacksmith.yml");
	}

	public double getRepairCost(ItemStack item) {
		if (item != null && !item.getType().equals(Material.AIR)) {
			double baseCost = config.getDouble("Base Cost." + item.getType().toString());
			if (baseCost != 0) {
				double exponent = config.getDouble("Exponent");
				double enchantModifier = config.getDouble("Enchantment Modifier");
				double multiplier = 1;
				int totalEnchants = 0;
				for (Entry<Enchantment, Integer> entry : item.getEnchantments().entrySet()) {
					totalEnchants += entry.getValue();
					if (entry.getKey().equals(Enchantment.DURABILITY)) {
						multiplier += entry.getValue() * config.getDouble("Unbreaking Multiplier");
					}
				}
				double durabilityPercent = item.getDurability() / (double) item.getType().getMaxDurability();
				double cost = multiplier * (enchantModifier * (double) totalEnchants + 1) * baseCost
						* Math.pow(durabilityPercent, exponent);
				return cost;
			}
		}
		return 0;
	}

	public boolean repair(Player player, ItemStack item) {
		double cost = getRepairCost(item);
		if (cost > 0) {
			if (plugin.getMoneyManager().subtractMoney(player, BigDecimal.valueOf(cost), MoneySource.REPAIR)) {
				item.setDurability((short) 0);
				return true;
			}
		}
		return false;
	}
}
