package com.viscunoti.minecraft.citizens;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.TraitFactory;
import net.citizensnpcs.api.trait.TraitInfo;

import org.bukkit.Bukkit;

import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class CitizensManager extends Manager {
	private VendorManager vendorManager;
	private BlacksmithManager blacksmithManager;

	public CitizensManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				TraitFactory tf = CitizensAPI.getTraitFactory();
				tf.registerTrait(TraitInfo.create(VendorTrait.class).withName("vendor"));
				tf.registerTrait(TraitInfo.create(AuctioneerTrait.class).withName("auctioneer"));
				tf.registerTrait(TraitInfo.create(BlacksmithTrait.class).withName("blacksmith"));
			}
		});

		vendorManager = new VendorManager(plugin, database);
		blacksmithManager = new BlacksmithManager(plugin, database);
		Bukkit.getPluginManager().registerEvents(vendorManager, plugin);
	}

	public VendorManager getVendorManager() {
		return vendorManager;
	}
	
	public BlacksmithManager getBlacksmithManager() {
		return blacksmithManager;
	}
}
