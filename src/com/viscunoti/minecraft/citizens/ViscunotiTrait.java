package com.viscunoti.minecraft.citizens;

import net.citizensnpcs.api.trait.Trait;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.viscunoti.minecraft.ViscunotiPlugin;

public abstract class ViscunotiTrait extends Trait {
	public ViscunotiPlugin plugin;

	protected ViscunotiTrait(String name) {
		super(name);
		plugin = (ViscunotiPlugin) Bukkit.getPluginManager().getPlugin("ViscunotiMC");
	}

	protected void whisper(Player player, String message) {
		if (player != null && message != null && !message.isEmpty())
			player.sendMessage("<" + getNPC().getName() + "> " + message);
	}
}
