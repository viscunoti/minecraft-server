package com.viscunoti.minecraft;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;
import com.viscunoti.minecraft.areas.AreaCommandManager;
import com.viscunoti.minecraft.areas.AreaManager;
import com.viscunoti.minecraft.areas.homes.HomeCommandManager;
import com.viscunoti.minecraft.areas.homes.HomeManager;
import com.viscunoti.minecraft.auctionhouse.AuctionHouseCommandManager;
import com.viscunoti.minecraft.auctionhouse.AuctionHouseManager;
import com.viscunoti.minecraft.citizens.CitizensManager;
import com.viscunoti.minecraft.combatlogging.CombatTrackingManager;
import com.viscunoti.minecraft.database.Database;
import com.viscunoti.minecraft.enderchestdeath.EnderchestDeathCommandManager;
import com.viscunoti.minecraft.enderchestdeath.EnderchestDeathManager;
import com.viscunoti.minecraft.entities.EntityCommandManager;
import com.viscunoti.minecraft.entities.ViscunotiEntityManager;
import com.viscunoti.minecraft.info.InfoCommandManager;
import com.viscunoti.minecraft.info.InfoManager;
import com.viscunoti.minecraft.info.MOTDCommandManager;
import com.viscunoti.minecraft.info.MOTDManager;
import com.viscunoti.minecraft.mail.MailCommandManager;
import com.viscunoti.minecraft.mail.MailManager;
import com.viscunoti.minecraft.menus.MenuManager;
import com.viscunoti.minecraft.money.MoneyCommandManager;
import com.viscunoti.minecraft.money.MoneyManager;
import com.viscunoti.minecraft.spells.SpellCastManager;
import com.viscunoti.minecraft.stats.DeathLogger;
import com.viscunoti.minecraft.stats.PlayerStatsCommandManager;
import com.viscunoti.minecraft.stats.PlayerStatsManager;
import com.viscunoti.minecraft.warps.WarpCommandManager;
import com.viscunoti.minecraft.warps.WarpManager;

public class ViscunotiPlugin extends JavaPlugin {

	private Database database;
	private FileConfiguration config;
	private Map<String, CommandManager> commandManagers = Maps.newHashMap();

	private PlayerStatsManager playerStatsManager;
	private ViscunotiEntityManager entityManager;
	private MoneyManager moneyManager;
	private MailManager mailManager;
	private AreaManager areaManager;
	private EnderchestDeathManager enderchestDeathManager;
	private HomeManager homeManager;
	private SpellCastManager spellCastManager;
	private WarpManager warpManager;
	private MenuManager menuManager;
	private AuctionHouseManager auctionHouseManager;
	private CombatTrackingManager combatTrackingManager;
	private InfoManager infoManager;
	private CitizensManager citizensManager;
	private PropertiesManager propertiesManager;
	private MOTDManager motdManager;

	public void onEnable() {
		saveDefaultConfig();
		config = getConfig();
		try {
			if (config.contains("MySQL.Address"))
				database = new Database(config.getString("MySQL.Address"), config.getString("MySQL.Username"),
						config.getString("MySQL.Password"), config.getString("MySQL.Database"));
			else
				return;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		propertiesManager = new PropertiesManager(this, database);
		moneyManager = new MoneyManager(this, database);
		mailManager = new MailManager(this, database);
		areaManager = new AreaManager(this, database);
		enderchestDeathManager = new EnderchestDeathManager(this, database);
		homeManager = new HomeManager(this, database, areaManager);
		spellCastManager = new SpellCastManager(this, database);
		warpManager = new WarpManager(this, database);
		menuManager = new MenuManager(this, database);
		auctionHouseManager = new AuctionHouseManager(this, database, menuManager, moneyManager, mailManager);
		entityManager = new ViscunotiEntityManager(this, database);
		playerStatsManager = new PlayerStatsManager(this, database);
		combatTrackingManager = new CombatTrackingManager(this, database);
		infoManager = new InfoManager(this, database);
		citizensManager = new CitizensManager(this, database);
		motdManager = new MOTDManager(this, database);

		reload();

		entityManager.registerEntities();

		PluginManager pm = Bukkit.getPluginManager();
		// Manager listeners
		pm.registerEvents(mailManager, this);
		pm.registerEvents(areaManager, this);
		pm.registerEvents(enderchestDeathManager, this);
		pm.registerEvents(spellCastManager, this);
		pm.registerEvents(playerStatsManager, this);
		pm.registerEvents(menuManager, this);
		pm.registerEvents(combatTrackingManager, this);
		pm.registerEvents(motdManager, this);
		// Logging
		pm.registerEvents(new DeathLogger(database), this);

		PlayerStatsCommandManager playerStatsCommandManager = new PlayerStatsCommandManager(playerStatsManager);
		// Commands
		commandManagers.put("area", new AreaCommandManager(areaManager));
		commandManagers.put("money", new MoneyCommandManager(moneyManager));
		commandManagers.put("mail", new MailCommandManager(this, mailManager, moneyManager));
		commandManagers.put("chest", new EnderchestDeathCommandManager(spellCastManager));
		commandManagers.put("home", new HomeCommandManager(homeManager, spellCastManager));
		commandManagers.put("stat", playerStatsCommandManager);
		commandManagers.put("played", playerStatsCommandManager);
		commandManagers.put("warp", new WarpCommandManager(warpManager, spellCastManager));
		commandManagers.put("auction", new AuctionHouseCommandManager(auctionHouseManager));
		commandManagers.put("spawn", new EntityCommandManager(entityManager));
		commandManagers.put("info", new InfoCommandManager(this));
		commandManagers.put("motd", new MOTDCommandManager(motdManager));
		// Plugin ticks
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this,
				new ViscunotiTick(moneyManager, areaManager, spellCastManager), 1, 1);

		for (Player p : Bukkit.getOnlinePlayers()) {
			playerStatsManager.setPlaytimeTimestamp(p);
		}
	}

	public void onDisable() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			playerStatsManager.updatePlaytime(p, false);
		}
		database.awaitTermination(10);
		try {
			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		CommandManager commandManager = commandManagers.get(cmd);
		if (commandManager != null) {
			return commandManager.onCommand(sender, command, cmd, args);
		}
		if (cmd.equals("viscunoti") || cmd.equals("visc")) {
			if (args.length == 1 && args[0].equalsIgnoreCase("reload") && sender.hasPermission("viscunoti.reload")) {
				reload();
				sender.sendMessage("Reloaded viscunoti config files.");
				return true;
			}
		}
		return false;
	}

	private void reload() {
		RecursiveManagerReloader reloader = new RecursiveManagerReloader(this, this.getClass());
		reloader.reloadManagers(this);
	}

	public YamlConfiguration getCustomConfig(String name) {
		return getCustomConfig(name, true);
	}

	public YamlConfiguration getCustomConfig(String name, boolean setDefaults) {
		File configFile = new File(getDataFolder(), name);
		YamlConfiguration defaultConfig = null;
		try {
			Reader reader = new InputStreamReader(getResource(name), "UTF8");
			defaultConfig = YamlConfiguration.loadConfiguration(reader);
			reader.close();
			if (!configFile.exists()) {
				InputStream in = this.getResource(name);
				OutputStream out = new FileOutputStream(configFile);
				IOUtils.copy(in, out);
				in.close();
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
		if (defaultConfig != null && setDefaults)
			config.setDefaults(defaultConfig);
		return config;
	}

	public String getFileContents(String name) {
		File file = new File(getDataFolder(), name);
		if (!file.exists()) {
			try {
				InputStream in = this.getResource(name);
				OutputStream out = new FileOutputStream(file);
				IOUtils.copy(in, out);
				in.close();
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String ret = null;
		try {
			InputStream in = new FileInputStream(file);
			ret = IOUtils.toString(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public Database getDatabaseConnection() {
		return database;
	}

	public PlayerStatsManager getPlayerStatsManager() {
		return playerStatsManager;
	}

	public ViscunotiEntityManager getEntityManager() {
		return entityManager;
	}

	public MoneyManager getMoneyManager() {
		return moneyManager;
	}

	public MailManager getMailManager() {
		return mailManager;
	}

	public AreaManager getAreaManager() {
		return areaManager;
	}

	public EnderchestDeathManager getEnderchestDeathManager() {
		return enderchestDeathManager;
	}

	public HomeManager getHomeManager() {
		return homeManager;
	}

	public SpellCastManager getSpellCastManager() {
		return spellCastManager;
	}

	public WarpManager getWarpManager() {
		return warpManager;
	}

	public MenuManager getMenuManager() {
		return menuManager;
	}

	public AuctionHouseManager getAuctionHouseManager() {
		return auctionHouseManager;
	}

	public CombatTrackingManager getCombatTrackingManager() {
		return combatTrackingManager;
	}

	public InfoManager getInfoManager() {
		return infoManager;
	}

	public CitizensManager getCitizensManager() {
		return citizensManager;
	}

	public PropertiesManager getPropertiesManager() {
		return propertiesManager;
	}
}
