package com.viscunoti.minecraft.geometry;

import java.util.List;
import java.util.Map;

import org.bukkit.Location;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Converted http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html to java
 */
public class IcoSphereCreator {
	private List<Point3D> data = Lists.newArrayList();
	private Map<Long, Integer> middlePointIndexCache = Maps.newHashMap();
	private int index = 0;

	private int addVertex(Point3D p) {
		double length = p.length();
		p.x = p.x / length;
		p.y = p.y / length;
		p.z = p.z / length;
		data.add(p);
		return index++;
	}

	private int getMiddlePoint(int p1, int p2) {
		boolean firstIsSmaller = p1 < p2;
		long smallerIndex = firstIsSmaller ? p1 : p2;
		long greaterIndex = firstIsSmaller ? p2 : p1;
		long key = (smallerIndex << 32) + greaterIndex;
		Integer ret = middlePointIndexCache.get(key);
		if (ret != null) {
			return ret;
		}

		Point3D point1 = data.get(p1);
		Point3D point2 = data.get(p2);
		Point3D middle = new Point3D((point1.x + point2.x) / 2, (point1.y + point2.y) / 2, (point1.z + point2.z) / 2);
		int i = addVertex(middle);
		return i;
	}

	public Location[] create(Location center, int recursionLevel) {
		double t = (1 + Math.sqrt(5)) / 2;
		//
		addVertex(new Point3D(-1, t, 0));
		addVertex(new Point3D(1, t, 0));
		addVertex(new Point3D(-1, -t, 0));
		addVertex(new Point3D(1, -t, 0));
		//
		addVertex(new Point3D(0, -1, t));
		addVertex(new Point3D(0, 1, t));
		addVertex(new Point3D(0, -1, -t));
		addVertex(new Point3D(0, 1, -t));
		//
		addVertex(new Point3D(t, 0, -1));
		addVertex(new Point3D(t, 0, 1));
		addVertex(new Point3D(-t, 0, -1));
		addVertex(new Point3D(-t, 0, 1));
		//
		// create 20 triangles of the icosahedron
		List<TriangleIndices> faces = Lists.newArrayList();

		// 5 faces around point 0
		faces.add(new TriangleIndices(0, 11, 5));
		faces.add(new TriangleIndices(0, 5, 1));
		faces.add(new TriangleIndices(0, 1, 7));
		faces.add(new TriangleIndices(0, 7, 10));
		faces.add(new TriangleIndices(0, 10, 11));

		// 5 adjacent faces
		faces.add(new TriangleIndices(1, 5, 9));
		faces.add(new TriangleIndices(5, 11, 4));
		faces.add(new TriangleIndices(11, 10, 2));
		faces.add(new TriangleIndices(10, 7, 6));
		faces.add(new TriangleIndices(7, 1, 8));

		// 5 faces around point 3
		faces.add(new TriangleIndices(3, 9, 4));
		faces.add(new TriangleIndices(3, 4, 2));
		faces.add(new TriangleIndices(3, 2, 6));
		faces.add(new TriangleIndices(3, 6, 8));
		faces.add(new TriangleIndices(3, 8, 9));

		// 5 adjacent faces
		faces.add(new TriangleIndices(4, 9, 5));
		faces.add(new TriangleIndices(2, 4, 11));
		faces.add(new TriangleIndices(6, 2, 10));
		faces.add(new TriangleIndices(8, 6, 7));
		faces.add(new TriangleIndices(9, 8, 1));

		for (int i = 0; i < recursionLevel; i++) {
			List<TriangleIndices> faces2 = Lists.newArrayList();
			for (TriangleIndices tri : faces) {
				int a = getMiddlePoint(tri.v1, tri.v2);
				int b = getMiddlePoint(tri.v2, tri.v3);
				int c = getMiddlePoint(tri.v3, tri.v1);
				faces2.add(new TriangleIndices(tri.v1, a, c));
				faces2.add(new TriangleIndices(tri.v2, b, a));
				faces2.add(new TriangleIndices(tri.v3, c, b));
				faces2.add(new TriangleIndices(a, b, c));
			}
			faces = faces2;
		}
		Location[] locations = new Location[data.size()];
		for (int i = 0; i < data.size(); i++) {
			Point3D point = data.get(i);
			locations[i] = new Location(center.getWorld(), center.getX() + point.x, center.getY() + point.y,
					center.getZ() + point.z);
		}
		return locations;
	}
}
