package com.viscunoti.minecraft.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class Database {
	private ExecutorService tp;
	private ComboPooledDataSource ds;

	public Database(String address, String username, String password, String db) throws ClassNotFoundException,
			SQLException {
		this(address, username, password, db, 25, 5);
	}

	public Database(String address, String user, String password, String db, int maxPoolSize, int maxIdlePoolSize)
			throws ClassNotFoundException, SQLException {
		tp = Executors.newCachedThreadPool();
		ds = new ComboPooledDataSource();
		try {
			ds.setDriverClass("com.mysql.jdbc.Driver");
			ds.setJdbcUrl("jdbc:mysql://" + address + "/" + db);
			ds.setUser(user);
			ds.setPassword(password);
			ds.setInitialPoolSize(3);
			ds.setMinPoolSize(3);
			ds.setMaxPoolSize(maxPoolSize);
			ds.setMaxConnectionAge(43200);
			ds.setMaxIdleTime(3600);
			ds.setMaxIdleTimeExcessConnections(1800);
			ds.setMaxAdministrativeTaskTime(60);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close(ResultSet rs) {
		Statement st = null;
		if (rs != null) {
			try {
				if (!rs.isClosed()) {
					try {
						st = rs.getStatement();
						rs.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						close(st);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void close(Statement st) {
		Connection conn = null;
		if (st != null) {
			try {
				if (!st.isClosed()) {
					try {
						conn = st.getConnection();
						st.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						conn.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void close() throws SQLException {
		tp.shutdown();
		ds.close();
	}

	public PreparedStatement prepareStatement(String sql) {
		try {
			Connection conn = ds.getConnection();
			return conn.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public synchronized ResultSet query(PreparedStatement st) {
		if (st != null)
			try {
				return st.executeQuery();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return null;
	}

	public void asyncQuery(String sql) {
		asyncQuery(sql, new Object[] {});
	}

	public void asyncQuery(String sql, Object[] args) {
		asyncQuery(sql, args, null);
	}

	public void asyncQuery(String sql, Object[] args, QueryRunnable onComplete) {
		Runnable async = new Runnable() {
			public void run() {
				try {
					Connection conn = ds.getConnection();
					PreparedStatement st = conn.prepareStatement(sql);
					for (int i = 0; i < args.length; i++) {
						st.setObject(i + 1, args[i]);
					}
					ResultSet rs = st.executeQuery();
					if (onComplete != null)
						onComplete.results = rs;
					if (onComplete != null)
						onComplete.run();
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		tp.submit(async);
	}

	public synchronized int update(PreparedStatement st) {
		if (st != null)
			try {
				return st.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return -1;
	}

	public void asyncUpdate(String sql) {
		asyncUpdate(sql, new Object[] {});
	}

	public void asyncUpdate(String sql, Object[] args) {
		asyncUpdate(sql, args, null);
	}

	public void asyncUpdate(String sql, Object[] args, UpdateRunnable onComplete) {
		Runnable async = new Runnable() {
			public void run() {
				try {
					Connection conn = ds.getConnection();
					PreparedStatement st = conn.prepareStatement(sql);
					for (int i = 0; i < args.length; i++) {
						st.setObject(i + 1, args[i]);
					}
					int rowsAffected = st.executeUpdate();
					conn.close();
					if (onComplete != null)
						onComplete.rowsAffected = rowsAffected;
					if (onComplete != null)
						onComplete.run();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		tp.submit(async);
	}

	public synchronized boolean execute(PreparedStatement st) {
		if (st != null)
			try {
				return st.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return false;
	}

	public void asyncExecute(String sql) {
		asyncExecute(sql, new Object[] {});
	}

	public void asyncExecute(String sql, Object[] args) {
		asyncExecute(sql, args, null);
	}

	public void asyncExecute(String sql, Object[] args, ExecuteRunnable onComplete) {
		Runnable async = new Runnable() {
			public void run() {
				try {
					Connection conn = ds.getConnection();
					PreparedStatement st = conn.prepareStatement(sql);
					for (int i = 0; i < args.length; i++) {
						st.setObject(i + 1, args[i]);
					}
					boolean status = st.execute();
					conn.close();
					if (onComplete != null)
						onComplete.status = status;
					if (onComplete != null)
						onComplete.run();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		tp.submit(async);
	}

	public void awaitTermination(long timeout) {
		tp.shutdown();
		try {
			tp.awaitTermination(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String toString() {
		try {
			return ds.getUser() + "@" + ds.getJdbcUrl() + " (" + ds.getNumBusyConnections() + " active, "
					+ ds.getNumIdleConnections() + " idle)";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
