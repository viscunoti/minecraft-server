package com.viscunoti.minecraft.money;

import java.math.BigDecimal;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Utils;

public class MoneyCommandManager extends CommandManager {
	protected MoneyManager moneyManager;

	public MoneyCommandManager(MoneyManager moneyManager) {
		this.moneyManager = moneyManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (args.length == 0) {
				p.sendMessage(ChatColor.GOLD + "You have " + Language.formatMoney(moneyManager.getMoney(p)) + ".");
				return true;
			} else if (args.length == 1) {
				if (args[0].equalsIgnoreCase("accept")) {
					MoneyTransfer transfer = moneyManager.acceptTransfer(p);
					if (transfer != null) {
						p.sendMessage(ChatColor.GOLD + "You received " + Language.formatMoney(transfer.getAmount())
								+ " from " + Bukkit.getOfflinePlayer(transfer.getFrom()).getName() + ".");
					} else {
						p.sendMessage(ChatColor.GRAY + "You have no pending transfers.");
					}
					return true;
				} else if (args[0].equalsIgnoreCase("cancel") || args[0].equalsIgnoreCase("decline")) {
					MoneyTransfer cancelled = moneyManager.cancelTransfer(p);
					if (cancelled == null) {
						p.sendMessage(ChatColor.GRAY + "You have no pending transfers.");
					}
					return true;
				} else if (p.hasPermission("viscunoti.money.view")) {
					OfflinePlayer get = Utils.getOfflinePlayer(args[0]);

					if (get != null) {
						BigDecimal money = moneyManager.getMoney(get);
						p.sendMessage(ChatColor.GOLD + get.getName() + " has " + Language.formatMoney(money) + ".");
					} else
						p.sendMessage(ChatColor.GRAY + "Player not found.");
					return true;
				}
			} else if (args.length == 3) {
				if (args[0].equalsIgnoreCase("give") || args[0].equalsIgnoreCase("transfer")) {
					Player target = Bukkit.getPlayerExact(args[1]);
					if (target != null) {
						try {
							BigDecimal amount = new BigDecimal(args[2]);
							MoneyTransferStatus status = moneyManager.initiateTransfer(p, target, amount);
							if (status.equals(MoneyTransferStatus.INITIATED_TRANSFER))
								p.sendMessage(ChatColor.GOLD + "Initiated transfer of " + Language.formatMoney(amount)
										+ " to " + target.getName() + ".  (Expires in 30 seconds)");
							else if (status.equals(MoneyTransferStatus.ERROR_NOT_ENOUGH_MONEY))
								p.sendMessage(ChatColor.RED + "You don't have enough money.");
							else if (status.equals(MoneyTransferStatus.ERROR_IN_PROGRESS))
								p.sendMessage(ChatColor.RED
										+ "The player has a pending transfer.  Please try again within the next 30 seconds.");
							else if (status.equals(MoneyTransferStatus.ERROR_NEGATIVE_MONEY))
								p.sendMessage(ChatColor.RED + "You must trade more than " + Language.formatMoney(0)
										+ ".");
							return true;
						} catch (NumberFormatException e) {
							p.sendMessage(ChatColor.RED + "Please enter a valid number.");
							return false;
						}
					} else {
						p.sendMessage(ChatColor.RED + "The player could not be found.");
						return false;
					}
				}
			}
		}

		return false;
	}

}
