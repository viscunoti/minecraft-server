package com.viscunoti.minecraft.money;

import java.math.BigDecimal;
import java.util.UUID;

public class MoneyTransfer {
	private UUID id, from, to;
	private BigDecimal amount;

	public MoneyTransfer(UUID from, UUID to, BigDecimal money) {
		this.id = UUID.randomUUID();
		this.from = from;
		this.to = to;
		this.amount = money;
	}

	public UUID getFrom() {
		return from;
	}

	public UUID getTo() {
		return to;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public UUID getId() {
		return id;
	}
}
