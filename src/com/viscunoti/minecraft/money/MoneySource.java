package com.viscunoti.minecraft.money;

public enum MoneySource {
	TRANSFER, MOB_KILL, MAIL, SEND_MAIL, VENDOR, REPAIR, COMMAND, UNKNOWN, AUCTION_HOUSE_SALE, AUCTION_HOUSE_PURCHASE
}
