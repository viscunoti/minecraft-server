package com.viscunoti.minecraft.money;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalCause;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class MoneyManager extends Manager {
	private Cache<UUID, MoneyTransfer> pendingTransfers;
	private MoneyKillManager moneyKillManager;

	public MoneyManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
		moneyKillManager = new MoneyKillManager(plugin, database, this);
		Bukkit.getPluginManager().registerEvents(moneyKillManager, plugin);
		pendingTransfers = CacheBuilder.newBuilder().concurrencyLevel(16).expireAfterWrite(30, TimeUnit.SECONDS)
				.removalListener(new RemovalListener<UUID, MoneyTransfer>() {
					public void onRemoval(RemovalNotification<UUID, MoneyTransfer> notification) {
						if (notification.getCause().equals(RemovalCause.EXPIRED)) {
							MoneyTransfer transfer = notification.getValue();
							OfflinePlayer to = Bukkit.getOfflinePlayer(transfer.getTo());
							OfflinePlayer from = Bukkit.getOfflinePlayer(transfer.getFrom());
							addMoney(transfer.getFrom(), transfer.getAmount(), MoneySource.TRANSFER);
							logTransferEvent(MoneyTransferEventType.EXPIRE, transfer);
							String money = Language.formatMoney(transfer.getAmount());
							if (from.isOnline()) {
								((Player) from).sendMessage(ChatColor.GOLD + "Your transfer of " + money + " to "
										+ to.getName() + " has expired and your gold has been refunded.");
							}
							if (to.isOnline()) {
								((Player) to).sendMessage(ChatColor.GOLD + "The transfer of " + money + " from "
										+ from.getName() + " to you has expired.");
							}
						}
					}
				}).build();

	}

	public MoneyTransferStatus initiateTransfer(OfflinePlayer from, OfflinePlayer to, BigDecimal amount) {
		if (from != null && to != null & amount != null) {
			if (amount.compareTo(BigDecimal.ZERO) == 1) {
				MoneyTransfer transfer = pendingTransfers.getIfPresent(to.getUniqueId());
				if (transfer != null)
					return MoneyTransferStatus.ERROR_IN_PROGRESS;
				if (subtractMoney(from, amount, MoneySource.TRANSFER)) {
					transfer = new MoneyTransfer(from.getUniqueId(), to.getUniqueId(), amount);
					pendingTransfers.put(to.getUniqueId(), transfer);
					logTransferEvent(MoneyTransferEventType.INITIATE, transfer);

					if (to.isOnline())
						((Player) to)
								.sendMessage(ChatColor.GOLD
										+ from.getName()
										+ " is trying to transfer "
										+ Language.formatMoney(amount)
										+ " to you.  Type /money <accept|cancel> to accept or cancel the transfer.  (Expires in 30 seconds)");

					return MoneyTransferStatus.INITIATED_TRANSFER;
				} else
					return MoneyTransferStatus.ERROR_NOT_ENOUGH_MONEY;
			}
			return MoneyTransferStatus.ERROR_NEGATIVE_MONEY;
		}
		return MoneyTransferStatus.ERROR;
	}

	public MoneyTransfer cancelTransfer(OfflinePlayer to) {
		MoneyTransfer transfer = pendingTransfers.getIfPresent(to.getUniqueId());
		if (transfer != null) {
			pendingTransfers.invalidate(to.getUniqueId());
			OfflinePlayer from = Bukkit.getOfflinePlayer(transfer.getFrom());
			addMoney(transfer.getFrom(), transfer.getAmount(), MoneySource.TRANSFER);
			logTransferEvent(MoneyTransferEventType.CANCEL, transfer);

			String money = Language.formatMoney(transfer.getAmount());
			if (from.isOnline())
				((Player) from).sendMessage(ChatColor.GOLD + "Your transfer of " + money + " to " + to.getName()
						+ " has been cancelled and your gold has been refunded.");
			if (to.isOnline())
				((Player) to).sendMessage(ChatColor.GOLD + "The transfer of " + money + " from " + from.getName()
						+ " has been cancelled.");
			return transfer;
		}
		return null;
	}

	public MoneyTransfer acceptTransfer(OfflinePlayer player) {
		MoneyTransfer transfer = pendingTransfers.getIfPresent(player.getUniqueId());
		if (transfer != null) {
			addMoney(transfer.getTo(), transfer.getAmount(), MoneySource.TRANSFER);
			logTransferEvent(MoneyTransferEventType.ACCEPT, transfer);
			pendingTransfers.invalidate(player.getUniqueId());
			Player from = Bukkit.getPlayer(transfer.getFrom());
			if (from != null)
				from.sendMessage(ChatColor.GOLD + player.getName() + " has accepted your "
						+ Language.formatMoney(transfer.getAmount()) + ".");
			return transfer;
		} else
			return null;
	}

	public void cancelAllTransfers() {
		for (MoneyTransfer transfer : pendingTransfers.asMap().values()) {
			if (transfer != null && transfer.getTo() != null) {
				cancelTransfer(Bukkit.getOfflinePlayer(transfer.getTo()));
			}
		}
	}

	public BigDecimal getMoney(OfflinePlayer player) {
		BigDecimal money = null;
		if (player != null) {
			String sql = "SELECT money FROM money WHERE user_id = ?";
			PreparedStatement st = database.prepareStatement(sql);
			ResultSet rs = null;
			try {
				st.setString(1, player.getUniqueId().toString());
				rs = database.query(st);
				if (rs.next()) {
					money = rs.getBigDecimal("money");
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.close(rs);
				database.close(st);
			}
		}
		return money == null ? BigDecimal.ZERO : money;

	}

	public void addMoney(OfflinePlayer player, BigDecimal amount) {
		if (player != null) {
			addMoney(player.getUniqueId(), amount);
		}
	}

	public void addMoney(OfflinePlayer player, double amount) {
		if (player != null) {
			addMoney(player.getUniqueId(), amount);
		}
	}

	public void addMoney(OfflinePlayer player, double amount, MoneySource source) {
		if (player != null) {
			addMoney(player.getUniqueId(), amount, source);
		}
	}

	public void addMoney(OfflinePlayer player, BigDecimal amount, MoneySource source) {
		if (player != null) {
			addMoney(player.getUniqueId(), amount, source);
		}
	}

	public void addMoney(UUID playerId, double amount) {
		addMoney(playerId, amount, MoneySource.UNKNOWN);
	}

	public void addMoney(UUID playerId, BigDecimal amount) {
		addMoney(playerId, amount, MoneySource.UNKNOWN);
	}

	public void addMoney(UUID playerId, double amount, MoneySource source) {
		addMoney(playerId, BigDecimal.valueOf(amount), source);
	}

	public void addMoney(UUID playerId, BigDecimal amount, MoneySource source) {
		if (playerId != null && amount != null) {
			String sql = "INSERT INTO money (user_id, money) VALUES (?, GREATEST(?, 0)) ON DUPLICATE KEY UPDATE money = GREATEST(money + ?, 0)";
			database.asyncUpdate(sql, new Object[] { playerId.toString(), amount, amount });
			logMoney(playerId, source, amount);
		}
	}

	public boolean subtractMoney(OfflinePlayer player, BigDecimal amount) {
		return subtractMoney(player, amount, MoneySource.UNKNOWN);
	}

	public boolean subtractMoney(OfflinePlayer player, BigDecimal amount, MoneySource source) {
		if (player != null) {
			return subtractMoney(player.getUniqueId(), amount, source);
		}
		return false;
	}

	public boolean subtractMoney(UUID playerId, BigDecimal amount) {
		return subtractMoney(playerId, amount, MoneySource.UNKNOWN);
	}

	public boolean subtractMoney(UUID playerId, BigDecimal amount, MoneySource source) {
		if (playerId != null && amount != null && amount.compareTo(BigDecimal.ZERO) == 1) {
			String sql = "UPDATE money SET money = money - ? WHERE user_id = ? AND money >= ?";
			PreparedStatement st = database.prepareStatement(sql);
			try {
				st.setBigDecimal(1, amount);
				st.setString(2, playerId.toString());
				st.setBigDecimal(3, amount);
				int rowsAffected = st.executeUpdate();
				if (rowsAffected == 1) {
					logMoney(playerId, source, amount.negate());
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.close(st);
			}
		}
		return false;
	}

	private void logMoney(UUID playerId, MoneySource source, BigDecimal money) {
		if (playerId != null && source != null && money != null) {
			String sql = "INSERT INTO money_log (user_id, source, money) VALUES (?, ?, ?)";
			database.asyncUpdate(sql, new Object[] { playerId.toString(), source.toString(), money });
		}
	}

	private void logTransferEvent(MoneyTransferEventType event, MoneyTransfer transfer) {
		if (event != null && transfer != null) {
			String sql = "INSERT INTO money_transactions (transaction_id, event, from_id, to_id, money) VALUES (?, ?, ?, ?, ?)";
			database.asyncUpdate(sql, new Object[] { transfer.getId().toString(), event.toString(),
					transfer.getFrom().toString(), transfer.getTo().toString(), transfer.getAmount() });
		}
	}

	public void tick() {
		pendingTransfers.cleanUp();
	}

	public MoneyKillManager getMoneyKillManager() {
		return moneyKillManager;
	}
}
