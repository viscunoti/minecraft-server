package com.viscunoti.minecraft.money;

import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import com.viscunoti.minecraft.Language;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;
import com.viscunoti.minecraft.entities.ViscunotiEntity;

public class MoneyKillManager extends Manager implements Listener {
	private MoneyManager moneyManager;
	private YamlConfiguration config;

	public MoneyKillManager(ViscunotiPlugin plugin, Database database, MoneyManager moneyManager) {
		super(plugin, database);
		this.moneyManager = moneyManager;
	}

	@EventHandler
	private void onEntityDeath(EntityDeathEvent e) {
		LivingEntity killed = e.getEntity();
		Player killer = killed.getKiller();
		String killedName = killed.getCustomName() != null ? killed.getCustomName() : killed.getName();
		String killedType;
		// Prepare killed type
		if (((CraftEntity) killed).getHandle() instanceof ViscunotiEntity) {
			killedType = ((ViscunotiEntity) ((CraftEntity) killed).getHandle()).getType().toString();
		} else {
			killedType = killed.getType().toString();
			if (killed instanceof Zombie && ((Zombie) killed).isVillager())
				killedType = "VILLAGER_" + killedType;
			else if (killed instanceof Skeleton && ((Skeleton) killed).getSkeletonType().equals(SkeletonType.WITHER))
				killedType = "WITHER_" + killedType;
			else if (killed instanceof Guardian && ((Guardian) killed).isElder())
				killedType = "ELDER_" + killedType;
		}
		double money = getMoneyReward(killedType);
		if (money == 0)
			return;
		if (isMoneySplit(killedType)) {
			// Split money between players
			Map<UUID, Double> killers = plugin.getCombatTrackingManager().getDamagers(killed);
			double totalDamage = plugin.getCombatTrackingManager().getDamageTaken(killed);
			for (Entry<UUID, Double> entry : killers.entrySet()) {
				OfflinePlayer killerPlayer = Bukkit.getOfflinePlayer(entry.getKey());
				double individualMoney = (entry.getValue() / totalDamage) * money;
				if (individualMoney > 0) {
					moneyManager.addMoney(killerPlayer, individualMoney);
					if (killerPlayer.isOnline())
						killerPlayer.getPlayer().sendMessage(
								ChatColor.GOLD + "You received " + Language.formatMoney(individualMoney)
										+ " for dealing " + entry.getValue() + " damage to " + killedName + ".");
				}
			}
		} else if (killer != null) {
			moneyManager.addMoney(killer, money, MoneySource.MOB_KILL);
			killer.sendMessage(ChatColor.GOLD + "You looted " + Language.formatMoney(money) + " from " + killedName
					+ ".");
		}
	}

	public void reload() {
		config = plugin.getCustomConfig("entity_kill_value.yml", false);
	}

	public boolean isMoneySplit(String mob) {
		return config.getBoolean("Entities." + mob + ".split", false);
	}

	public double getMoneyReward(String mob) {
		double money = 0;
		if (config.isConfigurationSection("Entities." + mob)) {
			money = config.getDouble("Entities." + mob + ".money", 0);
		} else {
			money = config.getDouble("Entities." + mob, 0);
		}
		double variation = config.getDouble("Variation") * money;
		money += Math.random() * variation - variation / 2d;
		return money;
	}
}
