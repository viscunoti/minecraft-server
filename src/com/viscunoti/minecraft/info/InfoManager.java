package com.viscunoti.minecraft.info;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import com.google.common.collect.Maps;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class InfoManager extends Manager {
	private Map<String, ItemStack> books = Maps.newHashMap();

	public InfoManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	public void reload() {
		books.clear();
		File folder = new File(plugin.getDataFolder().getPath() + File.separator + "info");
		if (folder.isDirectory()) {
			Pattern pattern = Pattern.compile("##page##", Pattern.CASE_INSENSITIVE);
			for (File f : folder.listFiles()) {
				try {
					String name = FilenameUtils.removeExtension(f.getName());
					String content = IOUtils.toString(new FileInputStream(f));
					content = content.replace("\r", "");
					String[] pages = pattern.split(content);
					ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
					BookMeta meta = (BookMeta) book.getItemMeta();
					meta.setTitle("Information: " + name);
					meta.setAuthor("Viscunoti Dev Team");
					meta.setPages(Arrays.asList(pages));
					book.setItemMeta(meta);
					books.put(name.toLowerCase(), book);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ItemStack getInfoBook() {
		return getInfoBook(plugin.getConfig().getString("Info.Default"));
	}

	public ItemStack getInfoBook(String name) {
		if (name != null)
			return books.get(name.toLowerCase());
		return null;
	}
}
