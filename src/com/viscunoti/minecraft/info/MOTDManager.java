package com.viscunoti.minecraft.info;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class MOTDManager extends Manager implements Listener {

	public MOTDManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	@EventHandler
	private void onPlayerJoin(PlayerJoinEvent e) {
		String motd = getMOTD();
		if (motd != null)
			e.getPlayer().sendMessage(ChatColor.BOLD + motd);
	}

	public String getMOTD() {
		return plugin.getPropertiesManager().getProperty("motd");
	}

	public void setMOTD(String motd) {
		plugin.getPropertiesManager().setProperty("motd", motd);
	}
}
