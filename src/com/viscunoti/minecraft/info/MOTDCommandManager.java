package com.viscunoti.minecraft.info;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.Utils;

public class MOTDCommandManager extends CommandManager {
	private MOTDManager motdManager;

	public MOTDCommandManager(MOTDManager motdManager) {
		this.motdManager = motdManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (args.length == 0) {
			String motd = motdManager.getMOTD();
			if (motd != null)
				sender.sendMessage(ChatColor.BOLD + motd);
			else
				sender.sendMessage(ChatColor.GRAY + "There is no message of the day.");
			return true;
		} else if (args.length >= 1 && sender.hasPermission("viscunoti.motd.edit")) {
			if ((args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("edit")) && args.length >= 2) {
				String motd = Utils.joinArgs(args, 1);
				motdManager.setMOTD(motd);
				sender.sendMessage("Message of the day set to:");
				sender.sendMessage(motd);
			} else if ((args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")) && args.length == 1) {
				motdManager.setMOTD(null);
				sender.sendMessage("Deleted message of the day.");
			}

			return true;
		}
		return false;
	}

}
