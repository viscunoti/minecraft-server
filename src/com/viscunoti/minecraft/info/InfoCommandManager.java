package com.viscunoti.minecraft.info;

import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.base.Joiner;
import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.ViscunotiPlugin;

public class InfoCommandManager extends CommandManager {
	private ViscunotiPlugin plugin;

	public InfoCommandManager(ViscunotiPlugin plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (cmd.equals("info")) {
				ItemStack book;
				if (args.length == 0) {
					book = plugin.getInfoManager().getInfoBook();
				} else {
					book = plugin.getInfoManager().getInfoBook(Joiner.on(' ').join(args));
				}
				if (book != null) {
					Map<Integer, ItemStack> overflow = p.getInventory().addItem(book);
					if (overflow.isEmpty())
						p.sendMessage("You have been given an info book.");
					else
						p.sendMessage(ChatColor.GRAY + "Your inventory is full.");
				} else {
					p.sendMessage(ChatColor.GRAY + "That info book does not exist.");
				}
				return true;
			}
		}
		return false;
	}
}
