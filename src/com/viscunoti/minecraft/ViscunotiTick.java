package com.viscunoti.minecraft;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;

public class ViscunotiTick implements Runnable {
	private List<Manager> managers;
	private long tick = 0;

	public ViscunotiTick(Manager... managers) {
		this.managers = Arrays.asList(managers);
	}

	public void addManager(Manager manager) {
		managers.add(manager);
	}

	public void removeManager(Manager manager) {
		managers.remove(manager);
	}

	public void run() {
		if (!Bukkit.getOnlinePlayers().isEmpty()) {
			tick++;
			for (Manager manager : managers) {
				if (tick % manager.getTickRate() == 0)
					manager.tick();
			}
		}
	}
}
