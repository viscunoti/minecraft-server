package com.viscunoti.minecraft.stats;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.Language;

public class PlayerStatsCommandManager extends CommandManager {
	private PlayerStatsManager playerStatsManager;

	public PlayerStatsCommandManager(PlayerStatsManager playerStatsManager) {
		this.playerStatsManager = playerStatsManager;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (cmd.equals("stat")) {
			if (args.length == 1 && sender instanceof Player || args.length == 2
					&& sender.hasPermission("viscunoti.stats.view")) {
				OfflinePlayer player = args.length == 2 ? Bukkit.getOfflinePlayer(args[1]) : (Player) sender;
				if (player != null) {
					sender.sendMessage(args[0] + " for " + player.getName() + ": "
							+ playerStatsManager.getStat(player, args[0]));
				} else {
					sender.sendMessage(ChatColor.GRAY + "Player not found.");
				}
				return true;
			}
		} else if (cmd.equalsIgnoreCase("played")) {
			if (args.length == 0 && sender instanceof Player || args.length == 1
					&& sender.hasPermission("viscunoti.stats.view")) {
				OfflinePlayer player = args.length == 1 ? Bukkit.getOfflinePlayer(args[0]) : (Player) sender;
				if (player != null) {
					long playtime = playerStatsManager.getStat(player, "play_time");
					if (player.isOnline()) {
						Player p = player.getPlayer();
						if (p.hasMetadata("playtime_timestamp"))
							playtime += System.currentTimeMillis() - p.getMetadata("playtime_timestamp").get(0).asLong();
					}
					String name = args.length == 0 ? "You" : player.getName();
					sender.sendMessage(name + " played for " + Language.formatDuration(playtime) + ".");
				} else {
					sender.sendMessage(ChatColor.GRAY + "Player not found.");
				}
				return true;
			}
		}
		return false;
	}
}
