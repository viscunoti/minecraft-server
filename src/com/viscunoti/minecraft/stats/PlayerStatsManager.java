package com.viscunoti.minecraft.stats;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class PlayerStatsManager extends Manager implements Listener {
	public PlayerStatsManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	@EventHandler
	private void onBlockBreak(BlockBreakEvent e) {
		addStat(e.getPlayer(), "blocks_broken", 1);
		addStat(e.getPlayer(), "blocks_broken", e.getBlock().getType().toString(), 1);
	}

	@EventHandler
	private void onBlockPlace(BlockPlaceEvent e) {
		addStat(e.getPlayer(), "blocks_placed", 1);
		addStat(e.getPlayer(), "blocks_placed", e.getBlock().getType().toString(), 1);
	}

	@EventHandler
	private void onPlayerChat(AsyncPlayerChatEvent e) {
		addStat(e.getPlayer(), "chat_messages_sent", 1);
	}

	@EventHandler
	private void onItemBreak(PlayerItemBreakEvent e) {
		addStat(e.getPlayer(), "tools_broken", 1);
		addStat(e.getPlayer(), "tools_broken", e.getBrokenItem().getType().toString(), 1);
	}

	@EventHandler
	private void onItemConsume(PlayerItemConsumeEvent e) {
		if (e.getItem().getType().isEdible()) {
			addStat(e.getPlayer(), "food_eaten", 1);
			addStat(e.getPlayer(), "food_eaten", e.getItem().getType().toString(), 1);
		} else {
			addStat(e.getPlayer(), "items_consumed", e.getItem().getType().toString(), 1);
		}
	}

	@EventHandler
	private void onLogin(PlayerLoginEvent e) {
		if (e.getResult().equals(Result.ALLOWED))
			loginLog(e.getPlayer(), "CONNECT_SUCCESS");
		else if (e.getResult().equals(Result.KICK_BANNED))
			loginLog(e.getPlayer(), "CONNECT_FAIL_BANNED");
		else if (e.getResult().equals(Result.KICK_FULL))
			loginLog(e.getPlayer(), "CONNECT_FAIL_FULL");
		else if (e.getResult().equals(Result.KICK_WHITELIST))
			loginLog(e.getPlayer(), "CONNECT_FAIL_WHITELIST");
		else if (e.getResult().equals(Result.KICK_OTHER))
			loginLog(e.getPlayer(), "CONNECT_FAIL");
		setPlaytimeTimestamp(e.getPlayer());
	}

	@EventHandler
	private void onQuit(PlayerQuitEvent e) {
		loginLog(e.getPlayer(), "DISCONNECT");
		updatePlaytime(e.getPlayer(), false);
	}

	public void setPlaytimeTimestamp(Player player) {
		if (player != null)
			player.setMetadata("playtime_timestamp", new FixedMetadataValue(plugin, System.currentTimeMillis()));
	}

	public void updatePlaytime(Player player) {
		updatePlaytime(player, true);
	}

	public void updatePlaytime(Player player, boolean update) {
		if (player.hasMetadata("playtime_timestamp")) {
			long currentTime = System.currentTimeMillis();
			long loginTime = player.getMetadata("playtime_timestamp").get(0).asLong();
			addStat(player, "play_time", currentTime - loginTime);
			if (update)
				player.setMetadata("playtime_timestamp", new FixedMetadataValue(plugin, currentTime));
		}
	}

	private void loginLog(OfflinePlayer player, String event) {
		if (player != null && event != null && !event.isEmpty())
			database.asyncUpdate("INSERT INTO login_log (user_id, event) VALUES (?, ?)", new Object[] {
					player.getUniqueId().toString(), event });
	}

	public long getStat(OfflinePlayer player, String stat) {
		if (player != null && stat != null && !stat.isEmpty()) {
			PreparedStatement st = null;
			ResultSet rs = null;
			try {
				st = database.prepareStatement("SELECT value FROM player_stats WHERE user_id = ? AND stat = ?");
				st.setString(1, player.getUniqueId().toString());
				st.setString(2, stat);
				rs = database.query(st);
				if (rs.next())
					return rs.getLong("value");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				database.close(rs);
				database.close(st);
			}
		}
		return 0;
	}

	public void addStat(OfflinePlayer player, String stat, long value) {
		if (player != null)
			addStat(player.getUniqueId(), stat, value);
	}

	public void addStat(UUID playerId, String stat, long value) {
		if (playerId != null && stat != null && !stat.isEmpty())
			database.asyncUpdate(
					"INSERT INTO player_stats (user_id, stat, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value = value + VALUES(value)",
					new Object[] { playerId.toString(), stat, value });
	}

	public void addStat(OfflinePlayer player, String stat, String data, long value) {
		if (player != null)
			addStat(player.getUniqueId(), stat, data, value);
	}

	public void addStat(UUID playerId, String stat, String data, long value) {
		if (playerId != null && stat != null && !stat.isEmpty() && data != null && !data.isEmpty())
			database.asyncUpdate(
					"INSERT INTO player_stats_advanced (user_id, stat, data, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value = value + VALUES(value)",
					new Object[] { playerId.toString(), stat, data, value });
	}
}
