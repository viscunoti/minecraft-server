package com.viscunoti.minecraft.stats;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.database.Database;

public class DeathLogger implements Listener {
	private Database database;

	public DeathLogger(Database database) {
		this.database = database;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onDeath(EntityDeathEvent e) {
		if (e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
			LivingEntity killed = e.getEntity();
			Entity killer = Utils.getLastDamager((EntityDamageByEntityEvent) killed.getLastDamageCause());

			Location loc = e.getEntity().getLocation();
			EntityDamageEvent lastDamage = e.getEntity().getLastDamageCause();
			String cause = null;
			if (lastDamage != null)
				cause = lastDamage.getCause().toString();

			String sql = "INSERT INTO kill_log (source, killer_type, killer_id, killed_type, killed_id, world, x, y, z) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
			Object[] args = new Object[9];
			args[0] = cause;
			if (killer != null) {
				args[1] = killer.getType().toString();
				args[2] = killer.getUniqueId().toString();
			}
			args[3] = killed.getType().toString();
			args[4] = killed.getUniqueId().toString();
			args[5] = loc.getWorld().getName();
			args[6] = loc.getX();
			args[7] = loc.getY();
			args[8] = loc.getZ();

			database.asyncUpdate(sql, args);
		}
	}
}
