package com.viscunoti.minecraft;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public abstract class CommandManager {
	public abstract boolean onCommand(CommandSender sender, Command command, String cmd, String[] args);
}
