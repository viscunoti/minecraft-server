package com.viscunoti.minecraft;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.minecraft.server.v1_8_R3.DamageSource;
import net.minecraft.server.v1_8_R3.EntityProjectile;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Utils {
	public static String formatTimestamp(long timestamp) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a z");
		return format.format(new Date(timestamp));
	}

	public static void giveItems(Player player, ItemStack... items) {
		if (items != null)
			for (ItemStack item : items) {
				if (player != null && item != null) {
					Map<Integer, ItemStack> overflow = player.getInventory().addItem(item);
					for (ItemStack i : overflow.values())
						player.getWorld().dropItem(player.getEyeLocation(), i);
				}
			}
	}

	public static List<String[]> getSignText(Block block) {
		List<String[]> signText = Lists.newArrayList();
		Location loc = block.getLocation();
		World world = loc.getWorld();
		int x = loc.getBlockX(), y = loc.getBlockY(), z = loc.getBlockZ();
		List<Block> blocks = Arrays.asList(world.getBlockAt(x + 1, y, z), world.getBlockAt(x - 1, y, z),
				world.getBlockAt(x, y, z + 1), world.getBlockAt(x, y, z - 1), world.getBlockAt(x, y + 1, z),
				world.getBlockAt(x, y - 1, z));
		for (Block b : blocks) {
			if (b.getState() instanceof Sign) {
				signText.add(((Sign) b.getState()).getLines());
			}
		}
		return signText;
	}

	public static boolean toBoolean(String string) {
		return string.equalsIgnoreCase("true") || string.equals("1") || string.equalsIgnoreCase("enabled")
				|| string.equalsIgnoreCase("yes");
	}

	public static Object deserializeBukkit(InputStream in) {
		BukkitObjectInputStream bis = null;
		Object ret = null;
		try {
			bis = new BukkitObjectInputStream(in);
			ret = bis.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public static byte[] serializeBukkit(Object obj) {
		byte[] bytes = null;
		ByteArrayOutputStream bos = null;
		BukkitObjectOutputStream out = null;
		try {
			bos = new ByteArrayOutputStream();
			out = new BukkitObjectOutputStream(bos);
			out.writeObject(obj);
			bytes = bos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				bos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return bytes;
	}

	private static Gson obfIgnoreGson = null;

	public static Gson getIgnoreDuplicateGson() {
		if (obfIgnoreGson == null)
			obfIgnoreGson = new GsonBuilder().setFieldNamingStrategy(new FieldNamingStrategy() {
				private Set<String> fields = Sets.newHashSet();

				public String translateName(Field f) {
					if (fields.contains(f.getName())) {
						return UUID.randomUUID().toString();
					}
					fields.add(f.getName());
					return f.getName();
				}
			}).create();
		return obfIgnoreGson;
	}

	public static Entity getLastDamager(EntityDamageByEntityEvent lastDamage) {
		if (lastDamage != null) {
			Entity damager = lastDamage.getDamager();
			EntityDamageByEntityEvent lastDamageByEntity = (EntityDamageByEntityEvent) lastDamage;
			if (lastDamageByEntity.getDamager() instanceof Projectile) {
				ProjectileSource ps = ((Projectile) lastDamageByEntity.getDamager()).getShooter();
				if (ps instanceof LivingEntity) {
					damager = (LivingEntity) ps;
				}
			} else {
				damager = lastDamageByEntity.getDamager();
			}
			return damager;
		}
		return null;
	}

	public static net.minecraft.server.v1_8_R3.Entity getLastDamager(DamageSource damagesource) {
		if (damagesource.getEntity() instanceof EntityProjectile
				&& damagesource.getEntity().projectileSource instanceof Entity)
			return (net.minecraft.server.v1_8_R3.Entity) damagesource.getEntity().projectileSource;
		else
			return damagesource.getEntity();
	}

	public static OfflinePlayer getOfflinePlayer(String string) {
		for (OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
			if (offlinePlayer.getName().equalsIgnoreCase(string)) {
				return offlinePlayer;
			}
		}
		return null;
	}

	public static ItemStack setName(ItemStack item, String name) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}

	public static String joinArgs(String[] args, int start) {
		return Joiner.on(' ').join(Arrays.copyOfRange(args, start, args.length));
	}
}
