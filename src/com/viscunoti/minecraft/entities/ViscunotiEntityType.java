package com.viscunoti.minecraft.entities;

import net.minecraft.server.v1_8_R3.Entity;

import com.viscunoti.minecraft.entities.enderdragonboss.EnderDragonBossEntityManager;
import com.viscunoti.minecraft.entities.enderdragonboss.EntityEnderDragonBoss;
import com.viscunoti.minecraft.entities.endermanminion.EndermanMinionEntityManager;
import com.viscunoti.minecraft.entities.endermanminion.EntityEndermanMinion;

public enum ViscunotiEntityType {
	ENDER_DRAGON_BOSS(EntityEnderDragonBoss.class, "EnderDragonBoss", 63, EnderDragonBossEntityManager.class), ENDERMAN_MINION(
			EntityEndermanMinion.class, "EndermanMinion", 58, EndermanMinionEntityManager.class);

	private Class<? extends Entity> clazz;
	private String name;
	private int id;
	private Class<? extends EntityManager> entityManager;

	private ViscunotiEntityType(Class<? extends Entity> clazz, String name, int id,
			Class<? extends EntityManager> entityManager) {
		this.clazz = clazz;
		this.name = name;
		this.id = id;
		this.entityManager = entityManager;
	}

	public Class<? extends Entity> getClazz() {
		return clazz;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public static ViscunotiEntityType getEntityType(String name) {
		for (ViscunotiEntityType e : ViscunotiEntityType.values()) {
			if (e.getName().equalsIgnoreCase(name)) {
				return e;
			}
		}
		return null;
	}

	public Class<? extends EntityManager> getEntityManagerClass() {
		return entityManager;
	}
}
