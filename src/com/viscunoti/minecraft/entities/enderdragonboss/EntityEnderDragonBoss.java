package com.viscunoti.minecraft.entities.enderdragonboss;

import java.util.Collection;

import net.minecraft.server.v1_8_R3.DamageSource;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityEnderDragon;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.World;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEnderman;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.viscunoti.minecraft.Utils;
import com.viscunoti.minecraft.entities.ViscunotiEntity;
import com.viscunoti.minecraft.entities.ViscunotiEntityType;
import com.viscunoti.minecraft.entities.endermanminion.EntityEndermanMinion;
import com.viscunoti.minecraft.geometry.IcoSphereCreator;

public class EntityEnderDragonBoss extends EntityEnderDragon implements ViscunotiEntity {

	private int firetick = 100;
	private Location diveloc = null;

	public EntityEnderDragonBoss(World world) {
		super(world);
	}

	public String getName() {
		return "Voblivion";
	}

	public void m() {
		try {
			if (this.isAlive()) {
				if (diveloc == null) {
					super.m();
					Collection<Player> players = world.getWorld().getPlayers();
					for (Player p : players) {
						Location loc = p.getLocation().clone();
						if (Math.abs(loc.getX() - locX) < 5 && loc.getY() < locY - 5 && Math.abs(loc.getZ() - locZ) < 5) {
							diveloc = loc;
						}
					}
					if (firetick <= 0) {
						firetick = 100 + (int) (Math.random() * 100);
						Location loc = getBukkitEntity().getLocation();
						for (float iYaw = loc.getYaw() - 20; iYaw < loc.getYaw() + 20; iYaw += 2) {
							for (float ia = loc.getPitch() / 15 - 15; ia < loc.getPitch() / 15 + 15; ia += 2) {
								double pitch = ((ia + 90) * Math.PI) / 180;
								double yaw = ((iYaw + 90) * Math.PI) / 180;
								double x = Math.sin(pitch) * Math.cos(yaw);
								double z = Math.sin(pitch) * Math.sin(yaw);
								double y = Math.cos(pitch);
								FallingBlock b = loc.getWorld().spawnFallingBlock(loc, Material.FIRE, (byte) 0);
								b.setDropItem(false);
								b.setVelocity(new Vector(x, y, z).multiply(-2));
							}
						}
					}
				} else {
					lastX = locX;
					lastY = locY;
					lastZ = locZ;
					lastYaw = this.yaw;
					lastPitch = this.pitch;
					double newX = locX - diveloc.getX() > 0 ? locX + 0.25 : locX - 0.1;
					double newY = locY - 1.2;
					double newZ = locZ - diveloc.getZ() < 0 ? locZ + 0.25 : locZ - 0.1;
					this.setPosition(newX, newY, newZ);
					if (newY <= diveloc.getY()) {
						diveloc.getWorld().createExplosion(diveloc, 5);
						IcoSphereCreator sphereCreator = new IcoSphereCreator();
						Location center = getBukkitEntity().getLocation();
						Location[] fireLocations = sphereCreator.create(center, 2);
						for (Location l : fireLocations) {
							FallingBlock fire = l.getWorld().spawnFallingBlock(l, Material.FIRE, (byte) 0);
							fire.setVelocity(new Vector(l.getX() - center.getX(), l.getY() - center.getY(), l.getZ()
									- center.getZ()).multiply(0.75));
						}
						diveloc = null;
					}
				}
				firetick--;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected boolean dealDamage(DamageSource damagesource, float f) {
		try {
			if (!damagesource.isExplosion()) {
				Entity damager = Utils.getLastDamager(damagesource);
				if (damager instanceof EntityLiving) {
					EntityLiving target = (EntityLiving) damager;
					CraftEntity targetBukkit = target.getBukkitEntity();
					Collection<org.bukkit.entity.Entity> nearbyEntities = damager.getBukkitEntity().getWorld()
							.getNearbyEntities(targetBukkit.getLocation(), 32, 32, 32);
					for (org.bukkit.entity.Entity aggroEntity : nearbyEntities) {
						if (((CraftEntity) aggroEntity).getHandle() instanceof EntityEndermanMinion) {
							Enderman enderman = (Enderman) aggroEntity;
							EntityEndermanMinion entityEnderman = (EntityEndermanMinion) ((CraftEnderman) enderman)
									.getHandle();
							enderman.setTarget((LivingEntity) targetBukkit);
							entityEnderman.attachedToPlayer = true;
							entityEnderman.disableDrops = true;
							enderman.setHealth(9);
							enderman.setMaxHealth(9);
						}
					}

				}
				return super.dealDamage(damagesource, f);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public ViscunotiEntityType getType() {
		return ViscunotiEntityType.ENDER_DRAGON_BOSS;
	}
}
