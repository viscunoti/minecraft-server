package com.viscunoti.minecraft.entities.enderdragonboss;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World.Environment;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.entities.EntityManager;

public class EnderDragonBossEntityManager extends EntityManager {
	public EnderDragonBossEntityManager(ViscunotiPlugin plugin) {
		super(plugin);
		this.isListener = true;
	}

	@EventHandler
	private void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (((CraftEntity) e.getDamager()).getHandle() instanceof EntityEnderDragonBoss) {
			if (e.getEntity().getType().equals(EntityType.PLAYER)) {
				Player p = (Player) e.getEntity();
				LivingEntity damager = (LivingEntity) e.getDamager();
				final int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
					public void run() {
						Location tp = damager.getLocation().clone();
						tp.setPitch(p.getLocation().getPitch());
						tp.setYaw(p.getLocation().getYaw());
						p.teleport(tp);
						p.setFallDistance(0);
					}
				}, 1, 1);
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						Bukkit.getScheduler().cancelTask(taskId);
						p.setVelocity(p.getVelocity().normalize());
					}
				}, 50);
			}
		}
	}

	@EventHandler
	private void onDeath(EntityDeathEvent e) {
		if (((CraftEntity) e.getEntity()).getHandle() instanceof EntityEnderDragonBoss) {
			Map<UUID, Double> damagers = plugin.getCombatTrackingManager().getDamagers(e.getEntity());
			for (UUID id : damagers.keySet()) {
				plugin.getPlayerStatsManager().addStat(id, "ender_dragon_boss_kills", 1);
			}
		}
	}

	@EventHandler
	private void onChunkLoad(ChunkUnloadEvent e) {
		if (e.getWorld().getEnvironment().equals(Environment.THE_END)) {
			Entity[] entities = e.getChunk().getEntities();
			for (Entity entity : entities) {
				if (((CraftEntity) entity).getHandle() instanceof EntityEnderDragonBoss) {
					LivingEntity livingEntity = (LivingEntity) entity;
					livingEntity.setHealth(livingEntity.getMaxHealth());
				}

			}
		}
	}
}
