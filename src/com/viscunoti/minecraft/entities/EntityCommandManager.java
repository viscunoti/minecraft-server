package com.viscunoti.minecraft.entities;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.viscunoti.minecraft.CommandManager;

public class EntityCommandManager extends CommandManager {

	private ViscunotiEntityManager entityManager;

	public EntityCommandManager(ViscunotiEntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (args.length == 1 && sender instanceof Player && sender.hasPermission("viscunoti.entities.spawn")) {
			Player p = (Player) sender;
			ViscunotiEntityType entityType = ViscunotiEntityType.getEntityType(args[0]);
			if (entityType != null) {
				entityManager.spawn(entityType, p.getLocation());
				p.sendMessage("Spawned " + entityType.getName() + ".");
				return true;
			}
		}
		return false;
	}
}
