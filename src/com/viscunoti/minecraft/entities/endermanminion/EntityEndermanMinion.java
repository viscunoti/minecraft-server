package com.viscunoti.minecraft.entities.endermanminion;

import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.DamageSource;
import net.minecraft.server.v1_8_R3.EntityEnderman;
import net.minecraft.server.v1_8_R3.World;

import com.viscunoti.minecraft.entities.ViscunotiEntity;
import com.viscunoti.minecraft.entities.ViscunotiEntityType;
import com.viscunoti.minecraft.entities.enderdragonboss.EntityEnderDragonBoss;

public class EntityEndermanMinion extends EntityEnderman implements ViscunotiEntity {

	public boolean disableDrops = false;

	public EntityEndermanMinion(World world) {
		super(world);
		this.fireProof = true;
	}

	public String getName() {
		return "Enderman Minion";
	}

	public boolean damageEntity(DamageSource damagesource, float f) {
		try {
			if (damagesource.isExplosion() || damagesource.getEntity() instanceof EntityEnderDragonBoss)
				return false;
			return super.damageEntity(damagesource, f);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean bR() {
		return (a(new BlockPosition(this.locX, getBoundingBox().b, this.locZ)) >= 0.0F);
	}

	protected void dropDeathLoot(boolean flag, int i) {
		if (!disableDrops)
			super.dropDeathLoot(flag, i);
	}

	public ViscunotiEntityType getType() {
		return ViscunotiEntityType.ENDERMAN_MINION;
	}
}
