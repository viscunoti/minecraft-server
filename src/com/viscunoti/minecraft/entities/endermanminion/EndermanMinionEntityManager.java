package com.viscunoti.minecraft.entities.endermanminion;

import java.lang.reflect.Field;
import java.util.List;

import net.minecraft.server.v1_8_R3.BiomeBase;
import net.minecraft.server.v1_8_R3.BiomeBase.BiomeMeta;

import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.entities.EntityManager;

public class EndermanMinionEntityManager extends EntityManager {
	public EndermanMinionEntityManager(ViscunotiPlugin plugin) {
		super(plugin);
	}

	public void init() {
		try {
			Field f = BiomeBase.class.getDeclaredField("at");
			f.setAccessible(true);
			List<BiomeMeta> monsters = (List<BiomeMeta>) f.get(BiomeBase.SKY);
			monsters.clear();
			monsters.add(new BiomeMeta(EntityEndermanMinion.class, 100, 4, 4));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
