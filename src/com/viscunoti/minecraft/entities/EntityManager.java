package com.viscunoti.minecraft.entities;

import org.bukkit.event.Listener;

import com.viscunoti.minecraft.ViscunotiPlugin;

public abstract class EntityManager implements Listener {
	protected boolean isListener = false;
	protected ViscunotiPlugin plugin;

	public EntityManager(ViscunotiPlugin plugin) {
		this.plugin = plugin;
	}

	public void init() {

	}

	public boolean isListener() {
		return isListener;
	}
}
