package com.viscunoti.minecraft.entities;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityTypes;
import net.minecraft.server.v1_8_R3.World;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;

import com.google.common.collect.Lists;
import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class ViscunotiEntityManager extends Manager {
	private List<EntityManager> entityManagers = Lists.newArrayList();

	public ViscunotiEntityManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}

	public CraftEntity spawn(ViscunotiEntityType entityType, Location location) {
		try {
			World world = ((CraftWorld) location.getWorld()).getHandle();
			Entity entity = entityType.getClazz().getConstructor(World.class).newInstance(world);
			entity.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(),
					location.getPitch());
			world.addEntity(entity);
			return entity.getBukkitEntity();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void registerEntities() {
		try {
			Field c = EntityTypes.class.getDeclaredField("c");
			Field d = EntityTypes.class.getDeclaredField("d");
			Field f = EntityTypes.class.getDeclaredField("f");
			Field g = EntityTypes.class.getDeclaredField("g");
			c.setAccessible(true);
			d.setAccessible(true);
			f.setAccessible(true);
			g.setAccessible(true);
			for (ViscunotiEntityType e : ViscunotiEntityType.values()) {
				try {
					((Map) c.get(null)).put(e.getName(), e.getClazz());
					((Map) d.get(null)).put(e.getClazz(), e.getName());
					((Map) f.get(null)).put(e.getClazz(), e.getId());
					((Map) g.get(null)).put(e.getName(), e.getId());
					EntityManager manager = e.getEntityManagerClass().getConstructor(ViscunotiPlugin.class)
							.newInstance(plugin);
					try {
						manager.init();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					if (manager.isListener())
						Bukkit.getPluginManager().registerEvents(manager, plugin);
					entityManagers.add(manager);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
