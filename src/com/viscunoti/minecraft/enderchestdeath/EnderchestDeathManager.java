package com.viscunoti.minecraft.enderchestdeath;

import java.util.Iterator;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.viscunoti.minecraft.Manager;
import com.viscunoti.minecraft.ViscunotiPlugin;
import com.viscunoti.minecraft.database.Database;

public class EnderchestDeathManager extends Manager implements Listener {

	public EnderchestDeathManager(ViscunotiPlugin plugin, Database database) {
		super(plugin, database);
	}
	
	@EventHandler
	private void onPlayerDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		Iterator<ItemStack> drops = e.getDrops().iterator();
		while(drops.hasNext()){
			ItemStack item = drops.next();
			Inventory enderChest = p.getEnderChest();
			Map<Integer, ItemStack> overflow = enderChest.addItem(item);
			if(overflow.isEmpty())
				drops.remove();
		}
	}

}
