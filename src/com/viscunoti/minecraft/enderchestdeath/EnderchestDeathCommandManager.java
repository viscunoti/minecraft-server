package com.viscunoti.minecraft.enderchestdeath;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.viscunoti.minecraft.CommandManager;
import com.viscunoti.minecraft.spells.SpellCastManager;

public class EnderchestDeathCommandManager extends CommandManager {
	private SpellCastManager spellCastManager;

	public EnderchestDeathCommandManager(SpellCastManager spellCastManager) {
		this.spellCastManager = spellCastManager;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			spellCastManager.cast(p, 60, new Runnable() {
				public void run() {
					p.openInventory(p.getEnderChest());
				}
			});
			p.sendMessage("Opening ender chest.  Do not move until the cast is complete.");
			return true;
		}
		return false;
	}

}
