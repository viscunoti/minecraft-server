CREATE TABLE `area_members` (
  `area_id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `area_properties` (
  `area_id` char(36) NOT NULL,
  `property` varchar(50) NOT NULL,
  `value` bit(1) NOT NULL DEFAULT b'0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`area_id`,`property`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `areas` (
  `area_id` char(36) NOT NULL,
  `owner_id` char(36) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `world` varchar(50) NOT NULL,
  `xmin` int(11) NOT NULL,
  `xmax` int(11) NOT NULL,
  `ymin` int(11) NOT NULL,
  `ymax` int(11) NOT NULL,
  `zmin` int(11) NOT NULL,
  `zmax` int(11) NOT NULL,
  `pvp` bit(1) NOT NULL DEFAULT b'0',
  `protected` bit(1) NOT NULL DEFAULT b'0',
  `enter_announcement` varchar(200) NOT NULL,
  `exit_announcement` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`area_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `owner` (`owner_id`),
  KEY `location` (`world`,`xmin`,`xmax`,`ymin`,`ymax`,`zmin`,`zmax`),
  KEY `protected` (`protected`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `auctions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) NOT NULL,
  `item` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `price` decimal(13,2) NOT NULL,
  `price_per_item` decimal(13,2) NOT NULL,
  `itemstack` blob NOT NULL,
  `itemstack_json` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `active` (`active`),
  KEY `item` (`item`),
  KEY `price` (`price_per_item`,`price`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `homes` (
  `user_id` char(36) NOT NULL,
  `area_id` char(36) NOT NULL,
  `world` varchar(50) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `z` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `horse_owners` (
  `horse` char(36) NOT NULL,
  `owner` char(36) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`horse`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `kill_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(50) NOT NULL,
  `killer_type` varchar(50) DEFAULT NULL,
  `killer_id` char(36) DEFAULT NULL,
  `killed_type` varchar(50) NOT NULL,
  `killed_id` char(36) NOT NULL,
  `world` varchar(50) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `z` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `source` (`source`),
  KEY `killer_type_id` (`killer_type`,`killer_id`),
  KEY `killed_type_id` (`killed_type`,`killed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) NOT NULL,
  `event` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_id` char(36) NOT NULL,
  `from` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `itemstack` blob,
  `itemstack_json` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `opened_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`to_id`),
  KEY `user_id_created_at` (`to_id`,`opened_at`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `money` (
  `user_id` char(36) NOT NULL,
  `money` decimal(13,2) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `money_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` char(36) NOT NULL,
  `source` varchar(50) NOT NULL,
  `money` decimal(13,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id_source` (`user_id`,`source`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `money_transactions` (
  `transaction_id` char(36) NOT NULL,
  `event` varchar(50) NOT NULL,
  `from_id` char(36) NOT NULL,
  `to_id` char(36) NOT NULL,
  `money` decimal(13,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`transaction_id`,`event`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `player_stats` (
  `user_id` char(36) NOT NULL,
  `stat` varchar(50) NOT NULL,
  `value` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`stat`),
  KEY `stat_value` (`stat`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `player_stats_advanced` (
  `user_id` char(36) NOT NULL,
  `stat` varchar(50) NOT NULL,
  `data` varchar(50) NOT NULL,
  `value` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`stat`,`data`),
  KEY `stat_data_value` (`stat`,`data`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `properties` (
  `key` varchar(50) NOT NULL,
  `value` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `server_stats` (
  `stat` varchar(50) NOT NULL,
  `value` decimal(13,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`stat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `vendored_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor` varchar(50) NOT NULL,
  `player` char(36) NOT NULL,
  `item` blob NOT NULL,
  `item_json` text NOT NULL,
  `price` decimal(13,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `player` (`player`),
  KEY `vendor` (`vendor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `warps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `world` varchar(50) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `z` double NOT NULL,
  `permission` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;