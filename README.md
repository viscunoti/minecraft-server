### How do I get set up? ###

Git (Version control):

* Download and install Git: https://git-scm.com/downloads
* Open Git Bash
* git config --global user.name "Firstname Lastname"
* git config git config --global user.email "email@example.com"
* Set up SSH for git: https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git
* Navigate to the correct directory and run "git clone git@bitbucket.org:viscunoti/minecraft-server.git"

Java:

* JRE: https://java.com/en/download/manual.jsp
* JDK (includes JRE): http://www.oracle.com/technetwork/java/javase/downloads/index.html

Spigot (Minecraft server):

* Download the latest BuildTools.jar from http://www.spigotmc.org/
* In git bash, run "java -jar BuildTools.jar"

MySQL (Database) on Windows:

* Download and install MySQL Server from https://www.mysql.com/

MySQL (Database) on Debian/Ubuntu:

* apt-get install mysql-server-5.6

Plugin:

* Create a MySQL database "CREATE DATABASE minecraft"
* Execute all queries in "init_db.sql" to create all tables in database
* To start the server, run "java -Xms512M -Xmx2G -jar spigot.jar"
* Start the server to generate config files, then stop it.
* Edit the ViscunotiMC config file and set the database information.

Recommended Software:

* Eclipse IDE for Java Developers: https://eclipse.org/downloads/
* MySQL Workbench (GUI for MySQL): https://www.mysql.com/products/workbench/
* Notepad++ (for editing YML files): https://notepad-plus-plus.org/

### Contribution guidelines ###
* Before working on anything non-trivial, there should be a ticket assigned to you for it.  If one does not exist, create it.
* Create a branch off of develop and submit a pull request when you are finished.


### Tickets ###

Kinds:

* bug: Self explanatory
* enhancement: New feature
* proposal: Request to do something, we will decide at a later date whether or not we actually want to do it.
* task: Anything other than a feature or bug.  For example: "Check logs to determine player broke the rules and ban them"

Priorities:

1. Blocker: Server outage or game breaking bug that is actively being used.
2. Critical: Game breaking bug that is not actively being used or was not discovered yet.
3. Major: Bugs that are not game breaking but are affecting many players on the server, features that are requested often by many players on the server.
4. Minor: Bugs that affect less than half of the players on the server, major features that are planned for the next version.
5. Trivial: Bugs that affect only a few players on the server, features requested by a single player.

Workflow:

* new: Ticket is not in progress.  Assign to developer or viscunoti.
* open: Ticket is in progress.  Assign to the developer who is working on the ticket.
* on hold: Something else has higher priority so this will be done later.  Leave current assignee or assign to viscunoti.
* resolved: Developer finished their work and it needs to be tested by QA.  Assign to QA or viscunoti.
* duplicate: Same thing as another ticket.  Unassign.
* invalid: Feature already exists, bug does not exist, etc.  Unassign.
* wontfix: It was decided that this ticket would not be completed.  Unassign.
* closed: Ticket is complete.  Unassign.